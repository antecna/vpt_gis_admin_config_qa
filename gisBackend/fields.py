import pytest
import requests
import json
from GIS.Gis.loginn import url
from GIS.Gis.loginn import header
from GIS.Gis.loginn import headers
from GIS.Gis.parametertables import getall
from GIS.Gis.parametertables import get_id_parameter_table
from GIS.Gis.parametertables  import data


def get_all_fields_by_id_parameter_tables_auxiliary(idd):
    dat = json.dumps({

    })
    r = requests.request("GET", url + "parameter-tables/"+str(idd)+"/fields", headers=header, data=dat)
    t = r.json()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 200
    li = []
    for j in range(len(t["content"])):
        li.append(t["content"][j]["id"])
    return li



def get_all_fields_byId_parameter_tables_auxiliary_j(idd):
    dat = json.dumps({

    })
    r = requests.request("GET", url + "parameter-tables/"+str(idd)+"/fields", headers=header, data=dat)
    t = r.json()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 200
    return t


def data_insert_field(datatypeid,defaultwithformula,helptext,label,mandatory,name,optionalconstrains,plurallabel,relateddefaultvalueid,relatedsourcefieldid,relatedsourcetableid,requiredconstrains={}):
    data={
        "dataTypeId":datatypeid,
        "defaultWithFormula":defaultwithformula,
        "helpText":helptext,
        "label":label,
        "mandatory":mandatory,
        "name":name,
        "optionalConstraints":optionalconstrains,
        "pluralLabel":plurallabel,
        "relatedDefaultValueId":relateddefaultvalueid,
        "relatedSourceFieldId":relatedsourcefieldid,
        "relatedSourceTableId":relatedsourcetableid,
        "requiredConstraints":requiredconstrains
    }
    return data



def add_field(da, idd):
    dat = json.dumps(da)
    r = requests.request("POST", url + "parameter-tables/" + str(idd) + "/fields", headers=header, data=dat)
    return r


def get_id_fields_by_id_parameter_tables(idd):
    dat = json.dumps(data())
    r = requests.request("GET", url + "parameter-tables/"+str(idd)+"/fields", headers=header, data=dat)
    t = r.json()
    return t


def get_id_field(idd,dataname):

   m=get_id_fields_by_id_parameter_tables(idd)
   for i in range(len(m["content"])):
      if m["content"][i]["name"]==dataname:
         ime=m["content"][i]["id"]
   return ime


def delete_field(data_name,idd):
    datatypeid=get_id_field(idd,data_name)
    dat = json.dumps([
    ])
    r = requests.request("DELETE", url + "parameter-tables/" + str(idd) + "/fields/" + str(datatypeid), headers=header,
                         data=dat)
    return r


