import pytest
import requests
import json
from allure import severity, severity_level
from GIS.Gis.loginn import login
from GIS.Gis.loginn import url
from GIS.Gis.loginn import headers
from GIS.Gis.loginn import header


@severity(severity_level.CRITICAL)
def test_post_login():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    assert r.status_code == 200


#    ocekivana greska
@pytest.mark.xfail
def test_post_login_fail():
    dat = json.dumps({
        "password": "antecna",
        "username": "ceca"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    assert r.status_code == 200


#   ako ne vrati usera
@pytest.mark.xfail
def test_post_fail():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 200
    assert t["user"] == []


#  ako ne vrati id
@pytest.mark.xfail
def test_post_login_fail_id():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 200
    assert t["user"]["id"] == [None]


#  ako ne vrati firstName
@pytest.mark.xfail
def test_post_login_fail_first_name():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 200
    assert t["user"]["firstName"] == [None]


#  ako ne vrati lastName
@pytest.mark.xfail
def test_post_login_fail_last_name():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 200
    assert t["user"]["lastName"] == [None]


#  ako ne vrati username
@pytest.mark.xfail
def test_post_login_fail_username():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 200
    assert t["user"]["username"] == [None]


#  ako ne vrati userGroup
@pytest.mark.xfail
def test_post_login_fail_user_group():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 200
    assert t["user"]["userGroup"] == [None]


#  ako ne vrati client
@pytest.mark.xfail
def test_post_login_fail_client():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 200
    assert t["user"]["client"] == [None]


# ako ne vrati worksapceId
@pytest.mark.xfail
def test_post_login_fail_workspace_id():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 200
    assert t["user"]["workspaceId"] == [None]


# ako se prilikom logovanja zaboravi username
def test_login_miss_username():
    dat = json.dumps({

        "password": "bonus"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 400
    assert t["username"] == "Field is mandatory"


# ako se prilikom logovanja zaboravi sifra
def test_login_miss_password():
    dat = json.dumps({
        "username": "admin@planetsoft.eu"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 400
    assert t["password"] == "Field is mandatory"


# ako se prilikom logovanja zaboravi i sifra i username
def test_login_miss_username_password():
    dat = json.dumps({
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 400
    assert t["username"] == "Field is mandatory" and t["password"] == "Field is mandatory"


#  ako se unese osim passworda i username unese jos prizvoljnih polja, ignorisu se
def test_login_additional_fields():
    dat = json.dumps({
        "username": "admin@planetsoft.eu",
        "password": "bonus",
        "poljeone": "polje",
        "poljetwo": "polje"
    })
    r = requests.request("POST", url+"login", headers=headers, data=dat)
    assert r.status_code == 200


#  novi tokeni
def test_refresh():
    dat = json.dumps({
            "accessToken": login()[0],
            "refreshToken": login()[1]
    })
    r = requests.request("POST", url+"refresh", headers=headers, data=dat)
    assert r.status_code == 200
    t = r.json()
    return t["accessToken"], t["refreshToken"]


#  ako se prilikom refresh tokena zaboravi poslati refreshToken
def test_refresh_miss_rtoken():
    dat = json.dumps({
        "accessToken": login()[0]
    })
    r = requests.request("POST", url+"refresh", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 400
    assert t["refreshToken"] == "Field is mandatory"


#   akozaboravimo accessToken
def test_refresh_miss_atoken():
    dat = json.dumps({
         "refreshToken": login()[1]
    })
    r = requests.request("POST", url+"refresh", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 400
    assert t["accessToken"] == "Field is mandatory"


#   ako zaboravimo oba tokena poslati
def test_refresh_miss_tokens():
    dat = json.dumps({
    })
    r = requests.request("POST", url+"refresh", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 400
    assert t["accessToken"] == "Field is mandatory" and t["refreshToken"] == "Field is mandatory"


def test_get_version():
    dat = json.dumps({

    })
    r = requests.request("GET", url+"version", headers=headers, data=dat)
    t = r.json()
    assert r.status_code == 200
    assert t["applicationName"] == "gis-admin-config-api"


def test_data_types():
    dat = json.dumps({
    })
    r = requests.request("GET", url+"data-types", headers=header, data=dat)
    assert r.status_code == 200


@pytest.mark.xfail
def test_data_types_fail():
    dat = json.dumps({
    })
    r = requests.request("GET", url+"data-types", headers=headers, data=dat)
    assert r.status_code == 401


@pytest.mark.skip
def test_max_length():
    dat = json.dumps({
    })
    r = requests.request("GET", url+"data-types", headers=header, data=dat)
    t = r.json()
    assert r.status_code == 200
    for i in range(len(t)):
        assert t[i]["requiredConstraints"]["max_length"] >= t[i]["requiredConstraints"]["min_length"]


def test_max_length_char_field():
    dat = json.dumps({
    })

    r = requests.request("GET", url+"data-types", headers=header, data=dat)
    t = r.json()

    assert r.status_code == 200
    for i in range(len(t)):
        if t[i]["label"] == "Char Field":
            assert t[i]["requiredConstraints"]["max_length"] == 80


def test_max_digit_and_decimal_places():
    dat = json.dumps({
    })
    r = requests.request("GET", url + "data-types", headers=header, data=dat)
    t = r.json()

    assert r.status_code == 200
    for i in range(len(t)):
        if t[i]["label"] == "Decimal Field" and t[i]["displayType"] == "decimal":
            assert t[i]["requiredConstraints"]["max_digits"] == 9
            assert t[i]["requiredConstraints"]["decimal_places"] == 7


@pytest.mark.xfail
def test_max_digit_and_decimal_places_fail():
    dat = json.dumps({
    })
    r = requests.request("GET", url + "data-types", headers=header, data=dat)
    t = r.json()

    assert r.status_code == 200
    for i in range(len(t)):
        if t[i]["label"] == "Decimal Field" and t[i]["displayType"] == "decimal":
            assert t[i]["requiredConstraints"]["max_digits"] == 5
            assert t[i]["requiredConstraints"]["decimal_places"] == 9


def test_max_length_text_field():
    dat = json.dumps({
    })
    r = requests.request("GET", url + "data-types", headers=header, data=dat)
    t = r.json()

    assert r.status_code == 200
    for i in range(len(t)):
        if t[i]["label"] == "Text Field" and t[i]["displayType"] == "textarea":
            assert t[i]["requiredConstraints"]["max_length"] == 500


@pytest.mark.xfail
def test_max_length_text_field_fail():
    dat = json.dumps({
    })
    r = requests.request("GET", url + "data-types", headers=header, data=dat)
    t = r.json()

    assert r.status_code == 200
    for i in range(len(t)):
        if t[i]["label"] == "Text Field" and t[i]["displayType"] == "textarea":
            assert t[i]["requiredConstraints"]["max_length"] == 501


def test_autonumberfield_padding():
    dat = json.dumps({
    })
    r = requests.request("GET", url + "data-types", headers=header, data=dat)
    t = r.json()

    assert r.status_code == 200
    for i in range(len(t)):
        if t[i]["label"] == "Auto Number Field" and t[i]["displayType"] == "autonumber":
            assert t[i]["requiredConstraints"]["padding"] == 0


def test_slider_integer_field():
    dat = json.dumps({
    })
    r = requests.request("GET", url + "data-types", headers=header, data=dat)
    t = r.json()

    assert r.status_code == 200
    for i in range(len(t)):
        if t[i]["label"] == "Integer Slider" and t[i]["displayType"] == "intslider":
            assert t[i]["requiredConstraints"]["min_value"] == 0
            assert t[i]["requiredConstraints"]["max_value"] == 100
            assert t[i]["requiredConstraints"]["step"] == 1


@pytest.mark.xfail
def test_slider_integer_field_fail():
    dat = json.dumps({
    })
    r = requests.request("GET", url + "data-types", headers=header, data=dat)
    t = r.json()

    assert r.status_code == 200
    for i in range(len(t)):
        if t[i]["label"] == "Integer Slider" and t[i]["displayType"] == "intslider":
            assert t[i]["requiredConstraints"]["min_value"] == -9
            assert t[i]["requiredConstraints"]["max_value"] == 101
            assert t[i]["requiredConstraints"]["step"] == 1


@pytest.mark.skip
def test_multislider_field_():
    dat = json.dumps({
    })
    r = requests.request("GET", url + "data-types", headers=header, data=dat)
    t = r.json()

    assert r.status_code == 200
    for i in range(len(t)):
        if t[i]["label"] == "Multi Select Field" and t[i]["displayType"] == "multiselect":
            assert t[i]["requiredConstraints"]["min_length"] >= 0
            assert t[i]["requiredConstraints"]["max_length"] >= t[i]["requiredConstraints"]["min_length"]
            assert t[i]["requiredConstraints"]["allow_empty"] is True
            if t[i]["requiredConstraints"]["min_length"] == 0:
                assert t[i]["requiredConstraints"]["allow_empty"] is False
