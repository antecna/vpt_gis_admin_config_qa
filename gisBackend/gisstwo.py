import time

import pytest
import requests
import json
from GIS.Gis.loginn import url
from GIS.Gis.loginn import header
from GIS.Gis.loginn import headers
from GIS.Gis.parametertables import getall
from GIS.Gis.parametertables import get_id_parameter_table
from GIS.Gis.fields import  get_all_fields_by_id_parameter_tables_auxiliary
from GIS.Gis.fields import  get_all_fields_byId_parameter_tables_auxiliary_j
from GIS.Gis.indexes import get_all_unique_indexes
from GIS.Gis.indexes import get_all_unique_indexes_j
from GIS.Gis.parametertables  import data_parameter_tables
from GIS.Gis.parametertables  import data_parameter_tables_false
from GIS.Gis.parametertables  import data
from GIS.Gis.fields  import data_insert_field
from GIS.Gis.fields  import add_field
from GIS.Gis.fields  import delete_field
from GIS.Gis.parametertables import get_ids_parameter_table
from GIS.Gis.parametertables import add_parameter_table
from GIS.Gis.parametertables import get_id_parameter_tablee
from GIS.Gis.parametertables import delete_parameter_tables
from GIS.Gis.indexes import post_index


#  get parameter tables
@pytest.mark.parametrize("pagge", (0, 1, 2, 3, 4, 5))
@pytest.mark.parametrize("sizze", (10, 20, 30, 40, 50, 60))
def test_get_all_parameter_tables(pagge, sizze):
    params = {'page': pagge,
              'size': sizze}
    r = requests.request("GET", url+"parameter-tables", headers=header, params=params)
    assert r.status_code == 200


@pytest.mark.xfail
def test_getall_one():
    params = {'page': 2,
              'size': 13}
    r = requests.request("GET", url+"parameter-tables", headers=header, params=params)
    t = r.json()
    assert r.status_code == 200
    assert t["content"] != []




def test_post_parameter_tables():
    data=data_parameter_tables(True, True, True, True, True,"hellio","ghfjk","cecinatabelica",True)
    dat=json.dumps(data)
    m = getall()
    k = 0
    for i in range(len(m["content"])):

        if data["name"] == m["content"][i]["name"]:
            k = k+1

    if k == 0 :
        r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
        assert r.status_code == 201
    else:
        r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
        assert r.status_code == 409


@pytest.mark.xfail
def test_post_parameter_tables_check():
    data=data_parameter_tables(True, True, True, True, True,"hellio","ghfjk","cecinatabelica",True)
    dat = json.dumps(data)
    m = getall()
    k = 0
    for i in range(len(m["content"])):
        print("data"+data["name"])
        print("sadrzaj"+m["content"][i]["name"])
        if data["name"] == m["content"][i]["name"]:

            k = k + 1
    print(k)
    if k > 0:
        r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
        assert r.status_code == 201


@pytest.mark.xfail
def test_post_parameter_tables_false():
    data=data_parameter_tables_false(True,True,False,False,True,"jjj",True)
    dat = json.dumps(data)
    r = requests.request("POST", url+"parameter-tables", headers=header, data=dat)
    t = r.json()
    assert r.status_code == 400

    assert t["name"] == "Field is mandatory" and t["label"] == "Field is mandatory"


@pytest.mark.xfail
def test_post_parameter_tables_wrong_character_label():
    data=data_parameter_tables(True,True,True,True,True,"/",";","aaa",True)
    dat = json.dumps(data)
    r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
    assert r.status_code == 201


@pytest.mark.xfail
@pytest.mark.parametrize("character", (',', '.', 'ž', 'š', 'ć', 'č', 'đ', '}', '{', '/', ';', '_', '[', ']', '#', '%', '^', '&', '*', '+', '-', 'A', 'B', 'C'))
def test_post_parameter_tables_wrong_two_character_first(character):
    data=data_parameter_tables(True,True,True,True,True,"/","aa",character+"a",True)
    dat= json.dumps(data)
    r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
    assert r.status_code == 201


@pytest.mark.xfail
@pytest.mark.parametrize("character", (',', '.', 'ž', 'š', 'ć', 'č', 'đ', '}', '{', '/', ';', '_', '[', ']', '#', '%', '^', '&', '*', '+', '-', 'A', 'B', 'C'))
def test_post_parameter_tables_wrong_two_character_second(character):
    data = data_parameter_tables(True, True, True, True, True, "/", "aa", "a"+character, True)
    dat=json.dumps(data)
    r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
    assert r.status_code == 201


@pytest.mark.xfail
def test_post_parameter_tables_character41():
    data=data_parameter_tables(True,True,True,True,True,"m","e"*41,"e"*41,True)
    dat=json.dumps(data)
    r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
    assert r.status_code == 201


@pytest.mark.xfail
@pytest.mark.parametrize("character", (',', '.', 'ž', 'š', 'ć', 'č', 'đ', '}', '{', '/', ';', '_', '[', ']', '#', '%', '^', '&', '*', '+', '-', 'A', 'B', 'C'))
def test_post_parameter_tables_wrong_character_name(character):
    data=data_parameter_tables(True,True,True,True,True,"/","aaaa", character, True)
    dat=json.dumps(data)
    r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
    assert r.status_code == 201


@pytest.mark.parametrize("character", (',', '.', 'ž', 'š', 'ć', 'č', 'đ', '}', '{', '/', ';', '_', '[', ']', '#', '%', '^', '&', '*', '+', '-', 'A', 'B', 'C'))
@pytest.mark.xfail
def test_post_parameter_tables_english_alphabet_check(character):
    data=data_parameter_tables(True,True,True,True,True,"/","english","english"+character,True)
    dat = json.dumps(data)
    r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
    assert r.status_code == 201


@pytest.mark.xfail
def test_post_parameter_tables_description():
    data=data_parameter_tables(True,True,True,True,True,"a","english","english",True)
    dat = json.dumps(data)
    r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
    assert r.status_code == 201



@pytest.mark.xfail
@pytest.mark.parametrize("character", (1, 2, 3, 4, 5, 6, 7, 8, 9, 0))
def test_first_last_character_in_name(character):

    m = getall()

    for i in range(len(m["content"])):
        assert ord(m["content"][i]["name"][0])== ord('_')
        assert  ord(m["content"][i]["name"][0]) == ord(character)
        assert m.status_code == 200


def test_get_parameter_tables_id():
    list_of_ids = get_ids_parameter_table()
    if len(list_of_ids) >= 1 :
     for i in range(len(list_of_ids)):
      urll = url+"parameter-tables/"+str(list_of_ids[i])
    r = requests.request("GET", urll, headers=header)
    assert r.status_code == 200



def test_put_parameter_tables():
    idd=get_id_parameter_table()
    urll = url+"parameter-tables/"+str(idd)
    data=data_parameter_tables(True,True,True,True,True,"hell","na","cecina_tabelicaaa",True)
    dat=json.dumps(data)
    r = requests.request("PUT", urll, headers=header, data=dat)
    assert r.status_code == 200



def test_get_all_fields_by_id_parameter_tables():
    idd=get_id_parameter_table()
    dat = json.dumps(data())
    r = requests.request("GET", url + "parameter-tables/"+str(idd)+"/fields", headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 200



def test_post_insert_field():
    idd=get_id_parameter_table()
    data=data_insert_field(6,"hhhhhhhhhh","hhhhhhhhhhh","jjjjii",False,"lool"*5, None,None, None,None,None,{"max_digits":9, "decimal_places": 7})
    dat = json.dumps(data)
    r = requests.request("POST", url + "parameter-tables/" + str(idd) + "/fields", headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 201


@pytest.mark.fail
def test_post_insert_field_fail():
    idd=get_id_parameter_table()
    data=data_insert_field(6,"hhhhhhhhhh","hgggh"*201,"jjjjii",False,"pghp",None,None,None,None,None,{"maxDigits": 9, "decimalPlaces": 7})
    dat = json.dumps(data)
    r = requests.request("POST", url + "parameter-tables/" + str(idd) + "/fields", headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 201



def test_put_update_field():
    idtables=get_id_parameter_table()
    idfield=get_all_fields_by_id_parameter_tables_auxiliary(idtables)[0]
    data=data_insert_field(6,"hhhhhhhhhh","hhhhhhhhhhh","jjjjii",False,"hepili", None,None,None,None,None,{"maxDigits": 9, "decimalPlaces": 7})
    dat = json.dumps(data)
    r = requests.request("PUT", url + "parameter-tables/"+str(idtables)+"/fields/"+str(idfield), headers=header, data=dat)
    assert r.status_code == 200


@pytest.mark.skip
def test_get_default_values_parameter_tables():

    listi = []
    m = getall()
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    dat = json.dumps({
    })
    r = requests.request("GET", url + "parameter-tables/1090/fields/58/default-values", headers=header, data=dat)
    assert r.status_code == 200


def test_get_all_unique_indexes():
    idd=get_id_parameter_table()
    dat = json.dumps(data())
    r = requests.request("GET", url + "parameter-tables/"+str(idd)+"/indexes", headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 200


def test_post_insert_index_one():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "ghfjk", "qqq", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code==201
    data_field = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "auutonuumbeer", None, None, None, None,
                             None, {"padding": 0})
    number_table = get_id_parameter_tablee("qqq")
    print("brtabele"+str(number_table))
    field= add_field(data_field, number_table)
    assert field.status_code == 201
    listt = get_all_fields_by_id_parameter_tables_auxiliary(number_table)
    print(listt)
    if len(listt) >= 1:
       dat = json.dumps([
           listt[0]
       ])
    r = requests.request("POST", url + "parameter-tables/" + str(number_table) + "/indexes", headers=header, data=dat)
    assert r.status_code == 201

    delete_table=delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_post_insert_index_two():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "ghfjk", "qqq", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    data_field_one = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "auutonuumbeerone", None, None,
                                   None, None,
                                   None, {"padding": 0})
    number_table = get_id_parameter_tablee("qqq")
    print("brtabele" + str(number_table))
    fieldone = add_field(data_field_one, number_table)
    assert fieldone.status_code == 201
    data_field_two = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumbertwo", None, None,
                                   None, None,
                                   None, {"padding": 0})
    fieldtwo = add_field(data_field_two, number_table)
    assert fieldtwo.status_code == 201
    listt = get_all_fields_by_id_parameter_tables_auxiliary(number_table)
    print(listt)
    if len(listt) >= 2:
        dat = json.dumps([
            listt[0],
            listt[1]
        ])
    r = requests.request("POST", url + "parameter-tables/" + str(number_table) + "/indexes", headers=header, data=dat)
    assert r.status_code == 201

    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_post_insert_index_three():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "ghfjk", "qqq", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    data_field_one = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "auutonuumbeerone", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    number_table = get_id_parameter_tablee("qqq")
    print("brtabele" + str(number_table))
    fieldone = add_field(data_field_one, number_table)
    assert fieldone.status_code == 201
    data_field_two = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumbertwo", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    fieldtwo = add_field(data_field_two, number_table)
    assert fieldtwo.status_code == 201
    data_field_three = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumberthree", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    fieldthree = add_field(data_field_three, number_table)
    assert fieldthree.status_code == 201
    listt = get_all_fields_by_id_parameter_tables_auxiliary(number_table)
    print(listt)
    if len(listt) >= 3:
        dat = json.dumps([
            listt[0],
            listt[1],
            listt[2]
        ])
    r = requests.request("POST", url + "parameter-tables/" + str(number_table) + "/indexes", headers=header, data=dat)
    assert r.status_code == 201

    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_post_insert_index_four():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "ghfjk", "qqq", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    data_field_one = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "auutonuumbeerone", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    number_table = get_id_parameter_tablee("qqq")
    print("brtabele" + str(number_table))
    fieldone = add_field(data_field_one, number_table)
    assert fieldone.status_code == 201
    data_field_two = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumbertwo", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    fieldtwo = add_field(data_field_two, number_table)
    assert fieldtwo.status_code == 201
    data_field_three = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumberthree",
                                         None,
                                         None,
                                         None, None,
                                         None, {"padding": 0})
    fieldthree = add_field(data_field_three, number_table)
    assert fieldthree.status_code == 201
    data_field_four = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumberfour",
                                         None,
                                         None,
                                         None, None,
                                         None, {"padding": 0})
    fieldfour = add_field(data_field_four, number_table)
    assert fieldfour.status_code == 201
    listt = get_all_fields_by_id_parameter_tables_auxiliary(number_table)
    print(listt)
    if len(listt) >= 4:
        dat = json.dumps([
            listt[0],
            listt[1],
            listt[2],
            listt[3]
        ])
    r = requests.request("POST", url + "parameter-tables/" + str(number_table) + "/indexes", headers=header, data=dat)
    assert r.status_code == 201

    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_post_insert_index_five():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "ghfjk", "qqq", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    data_field_one = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "auutonuumbeerone", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    number_table = get_id_parameter_tablee("qqq")
    print("brtabele" + str(number_table))
    fieldone = add_field(data_field_one, number_table)
    assert fieldone.status_code == 201
    data_field_two = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumbertwo", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    fieldtwo = add_field(data_field_two, number_table)
    assert fieldtwo.status_code == 201
    data_field_three = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumberthree",
                                         None,
                                         None,
                                         None, None,
                                         None, {"padding": 0})
    fieldthree = add_field(data_field_three, number_table)
    assert fieldthree.status_code == 201
    data_field_four = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumberfour",
                                        None,
                                        None,
                                        None, None,
                                        None, {"padding": 0})
    fieldfour = add_field(data_field_four, number_table)
    assert fieldfour.status_code == 201
    data_field_five = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumberfive",
                                        None,
                                        None,
                                        None, None,
                                        None, {"padding": 0})
    fieldfive = add_field(data_field_five, number_table)
    assert fieldfive.status_code == 201
    listt = get_all_fields_by_id_parameter_tables_auxiliary(number_table)
    print(listt)
    if len(listt) >= 5:
        dat = json.dumps([
            listt[0],
            listt[1],
            listt[2],
            listt[3],
            listt[4]
        ])
    r = requests.request("POST", url + "parameter-tables/" + str(number_table) + "/indexes", headers=header, data=dat)
    assert r.status_code == 201

    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200

@pytest.mark.xfail
def test_post_insert_index_fail_one():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "ghfjk", "qjqq", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("qjqq")
    dat = json.dumps([

    ])
    r = requests.request("POST", url + "parameter-tables/"+str(number_table)+"/indexes", headers=header, data=dat)
    assert r.status_code == 201
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


@pytest.mark.xfail
def test_post_insert_index_fail_two():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "ghfjk", "qtqq", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201


    data_field_one = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "auutonuumbeerone", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    number_table = get_id_parameter_tablee("qtqq")
    print("brtabele" + str(number_table))
    fieldone = add_field(data_field_one, number_table)
    assert fieldone.status_code == 201
    data_field_two = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumbertwo", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    fieldtwo = add_field(data_field_two, number_table)
    assert fieldtwo.status_code == 201
    data_field_three = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumberthree",
                                         None,
                                         None,
                                         None, None,
                                         None, {"padding": 0})
    fieldthree = add_field(data_field_three, number_table)
    assert fieldthree.status_code == 201
    data_field_four = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumberfour",
                                        None,
                                        None,
                                        None, None,
                                        None, {"padding": 0})
    fieldfour = add_field(data_field_four, number_table)
    assert fieldfour.status_code == 201
    data_field_five = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumberfive",
                                        None,
                                        None,
                                        None, None,
                                        None, {"padding": 0})
    fieldfive = add_field(data_field_five, number_table)
    assert fieldfive.status_code == 201
    data_field_six = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumbersix",
                                    None,
                                    None,
                                    None, None,
                                    None, {"padding": 0})
    fieldsix = add_field(data_field_six, number_table)
    assert fieldsix.status_code == 201
    listt = get_all_fields_by_id_parameter_tables_auxiliary(number_table)
    if len(listt) > 5:
        dat = json.dumps([
            listt[0],
            listt[1],
            listt[2],
            listt[3],
            listt[4],
            listt[5]
        ])
    r = requests.request("POST", url + "parameter-tables/" + str(number_table) + "/indexes", headers=header, data=dat)
    assert r.status_code == 201
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_delete_index():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "qtqqq", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201

    number_table = get_id_parameter_tablee("qtqqq")
    data_field_two = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumbertwo", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    fieldtwo = add_field(data_field_two, number_table)
    assert fieldtwo.status_code == 201
    post_index(number_table)
    listt = get_all_unique_indexes(number_table)
    assert len(listt) >= 1
    dat = json.dumps([
    ])
    r = requests.request("DELETE", url + "parameter-tables/" + str(number_table) + "/indexes/" + str(listt[0]), headers=header,
                         data=dat)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


@pytest.mark.xfail
def test_delete_index_xfail():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "qttt", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("qttt")
    dat = json.dumps([

    ])
    post_r = requests.request("POST", url + "parameter-tables/" + str(number_table) + "/indexes", headers=header, data=dat)
    assert post_r.status_code == 201
    listt = get_all_unique_indexes(number_table)
    if len(listt) < 1:
     r = requests.request("DELETE", url + "parameter-tables/" + str(number_table) + "/indexes/" + str(listt[0]),
                             headers=header, data=dat)
    r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def  test_delete_field():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "deletefield", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("deletefield")

    data_field_two = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "autonuumbertwo", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    fieldtwo = add_field(data_field_two, number_table)
    assert fieldtwo.status_code == 201
    post_index(number_table)
    all_fields = get_all_fields_by_id_parameter_tables_auxiliary(number_table)
    all_fields_json = get_all_fields_byId_parameter_tables_auxiliary_j(number_table)
    for i in range(len(all_fields_json["content"])):
        if all_fields_json["content"][i]["id"] == all_fields[0]:
            name = all_fields_json["content"][i]["name"]

    indexes_json = get_all_unique_indexes_j(number_table)
    listica = []

    for j in range(len(indexes_json["content"])):
        if name in all_fields_json["content"][j]["name"]:
            listica.append(j)
    assert len(listica) >= 1
    if len(listica) >= 1:
        dat = json.dumps([
        ])
        r = requests.request("DELETE", url + "parameter-tables/" + str(number_table) + "/fields/" + str(all_fields[0]),
                             headers=header, data=dat)
        assert r.status_code == 409
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_delete_field_successfully():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "deletefieldsuccessfully", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("deletefieldsuccessfully")

    data_field_two = data_insert_field(1, "hhhhhyyhhh", "hhhghffhhyyhhh", "mooojjuuu", False, "aautonuumbertwo", None,
                                       None,
                                       None, None,
                                       None, {"padding": 0})
    fieldtwo = add_field(data_field_two, number_table)
    assert fieldtwo.status_code == 201
    all_fields = get_all_fields_by_id_parameter_tables_auxiliary(number_table)
    all_fields_json = get_all_fields_byId_parameter_tables_auxiliary_j(number_table)
    for i in range(len(all_fields_json["content"])):
        if all_fields_json["content"][i]["id"] == all_fields[0]:
            name = all_fields_json["content"][i]["name"]

    indexes_json = get_all_unique_indexes_j(number_table)
    listica = []

    for j in range(len(indexes_json["content"])):
        if name in all_fields_json["content"][j]["name"]:
            listica.append(j)
    assert len(listica) < 1
    if len(listica) < 1:
        dat = json.dumps([
        ])
        r = requests.request("DELETE", url + "parameter-tables/" + str(number_table) + "/fields/" + str(all_fields[0]),
                             headers=header, data=dat)
        assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_published_parameter_tables():
    dat = json.dumps([
    ])
    r = requests.request("GET", url + "parameter-tables/published", headers=header, data=dat)
    assert r.status_code == 200


def test_add_and_delete_auto_number_field():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "autonumbertable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("autonumbertable")
    data=data_insert_field(1,"hhhhhyyhhh","hhhhffhhyyhhh","jjjffjii",False,"autonumber", None,None,None,None,None,{"padding":0})
    t=add_field(data, number_table)
    assert t.status_code==201
    r=delete_field("autonumber",number_table)
    assert r.status_code==200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_add_and_delete_boolean_field():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "booleantable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("booleantable")
    data = data_insert_field(3, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "boolean", None, None, None, None,None)
    t = add_field(data, number_table)
    assert t.status_code == 201
    r = delete_field("boolean", number_table)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_add_and_delete_date_field():

    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "datetable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("datetable")

    data = data_insert_field(4, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "date", None, None, None, None,None)
    t = add_field(data,number_table)
    assert t.status_code == 201
    r = delete_field("date", number_table)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_add_and_delete_date_time_field():

    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "datetimetable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("datetimetable")
    data = data_insert_field(5, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "dateandtime", None, None, None, None, None)
    t = add_field(data,number_table)
    assert t.status_code == 201
    r = delete_field("dateandtime", number_table)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


def test_add_and_delete_decimal_field():

    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "decimaltable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("decimaltable")
    data = data_insert_field(6, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "decimal", None, None, None, None,None,{ "max_digits": 9,"decimal_places": 7})
    t = add_field(data, number_table)
    m=t.json()
    assert json.loads(m["requiredConstraints"])['max_digits']<=276447232
    assert t.status_code == 201
    r = delete_field("decimal", number_table)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


@pytest.mark.skip
def test_add_and_delete_file_field():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(8, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "fille", None, None, None, None,None)
    t = add_field(data)
    assert t.status_code == 201
    r = delete_field("fille", idd)
    assert r.status_code == 200



def test_add_and_delete_number_field():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "numbertable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("numbertable")
    data = data_insert_field(8, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "number", None, None, None, None,None)
    t = add_field(data,number_table)
    assert t.status_code == 201
    r = delete_field("number", number_table)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


@pytest.mark.skip
def test_add_and_delete_image_field():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(10, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "image", None, None, None, None,None)
    t = add_field(data)
    assert t.status_code == 201
    r = delete_field("image", idd)
    assert r.status_code == 200


def test_add_and_delete_integer_field():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "integertable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("integertable")
    data = data_insert_field(10, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "integer", None, None, None, None,None)
    t = add_field(data, number_table)
    assert t.status_code == 201
    r = delete_field("integer", number_table)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


@pytest.mark.skip
def test_add_and_delete_integer_slider_field():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(12, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "integerslider", None, None, None, None,None,{"min_value": 0, "max_value": 100,"step": 1})
    t = add_field(data)
    assert t.status_code == 201
    r = delete_field("integerslider", idd)
    assert r.status_code == 200



def test_add_and_delete_char_field():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "chartable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("chartable")
    data = data_insert_field(14, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "char", None, None, None, None,None,{"max_length": 80})
    t = add_field(data,number_table)
    assert t.status_code == 201
    r = delete_field("char", number_table)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200



def test_add_and_delete_text_field():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "texttable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("texttable")
    data = data_insert_field(15, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "text", None, None, None, None,None,{"max_length": 500})
    t = add_field(data, number_table)
    assert t.status_code == 201
    r = delete_field("text", number_table)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200



def test_add_and_delete_time_field():
    data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "timetable", True)
    post_table = add_parameter_table(data_table)
    assert post_table.status_code == 201
    number_table = get_id_parameter_tablee("timetable")
    data = data_insert_field(16, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "time", None, None, None, None,None)
    t = add_field(data,number_table)
    assert t.status_code == 201
    r = delete_field("time", number_table)
    assert r.status_code == 200
    delete_table = delete_parameter_tables(number_table)
    assert delete_table.status_code == 200


@pytest.mark.skip
def test_add_and_delete_select_field():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(14, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "select", None, None, None, 5,1)
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("select", idd)
    assert r.status_code == 200


@pytest.mark.skip
def test_add_and_delete_multiselect_field():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(13, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "multiselect", None, None, None, 5,1, { "min_length":1,"max_length":2,"allow_empty":True})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("multiselect", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_auto_number_field_fail_one():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data=data_insert_field(2,"hhhhhyyhhh","hhhhffhhyyhhh","jjjffjii",False,"autonumber", None,None,None,None,None)
    t=add_field(data, idd)
    assert t.status_code==201
    r=delete_field("autonumber",idd)
    assert r.status_code==200


@pytest.mark.xfail
def test_add_and_delete_auto_number_field_fail_two():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data=data_insert_field(2,"hhhhhyyhhh","hhhhffhhyyhhh","jjjffjii",False,"autonumber", None,None,None,None,None,{"padding":-5})
    t=add_field(data, idd)
    assert t.status_code==201
    r=delete_field("autonumber",idd)
    assert r.status_code==200


@pytest.mark.decimalone
@pytest.mark.xfail
def test_add_and_delete_decimal_field_fail_one():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(7, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "decimal", None, None, None, None,None,{ "max_digits": 9})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("decimal", idd)
    assert r.status_code == 200


@pytest.mark.decimaltwo
@pytest.mark.xfail
def test_add_and_delete_decimal_field_fail_two():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(7, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "decimal", None, None, None, None,None,{ "decimal_places": 7})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("decimal", idd)
    assert r.status_code == 200



@pytest.mark.decimalthree
@pytest.mark.xfail
def test_add_and_delete_decimal_field_fail_three():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(7, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "decimal", None, None, None, None,None,{ "max_digits": 9,"decimal_places": 10})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("decimal", idd)
    assert r.status_code == 200


@pytest.mark.decimalfour
@pytest.mark.xfail
def test_add_and_delete_decimal_field_fail_four():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(7, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "decimal", None, None, None, None,None,{ "max_digits": 0,"decimal_places": -1})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("decimal", idd)
    assert r.status_code == 200


@pytest.mark.decimalfive
@pytest.mark.xfail
def test_add_and_delete_decimal_field_five():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(7, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "decimal", None, None, None, None,None,{ "max_digits": 10000000000000,"decimal_places": 7})
    t = add_field(data, idd)
    m=t.json()
    assert json.loads(m["requiredConstraints"])['max_digits']>1316134912
    assert t.status_code == 201
    r = delete_field("decimal", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_integer_slider_field_fail_one():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(12, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "integerslider", None, None, None, None,None,{"min_value": 0, "max_value": 100,"step": 101})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("integerslider", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_integer_slider_field_fail_two():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(12, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "integerslider", None, None, None, None,None,{"min_value": 0, "max_value": 0,"step": 0})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("integerslider", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_integer_slider_field_fail_missing_min():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(12, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "integerslider", None, None, None, None,None,{ "max_value": 0,"step": 0})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("integerslider", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_integer_slider_field_fail_missing_max():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(12, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "integerslider", None, None, None, None,None,{"min_value": 0, "step": 0})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("integerslider", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_integer_slider_field_fail_missing_step():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(12, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "integerslider", None, None, None, None,None,{"min_value": 0, "max_value": 0})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("integerslider", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_char_field_fail_one():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(15, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "char", None, None, None, None,None,{"max_length": 0})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("char", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_char_field_fail_missing_max():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(15, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "char", None, None, None, None,None)
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("char", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_text_field_fail():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(16, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "text", None, None, None, None,None,{"max_length": 0})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("text", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_text_field_fail_missing_max():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(16, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "text", None, None, None, None,None)
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("text", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_multiselect_field_fail_one():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(13, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "multiselect", None, None, None, 5,1, { "min_length":-1,"max_length":2,"allow_empty":True})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("multiselect", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_multiselect_field_fail_two():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(13, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "multiselect", None, None, None, 5,1, { "min_length":1,"max_length":0,"allow_empty":True})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("multiselect", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_multiselect_field_fail_three():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(13, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "multiselect", None, None, None, 5,1, { "min_length":0,"max_length":1,"allow_empty":True})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("multiselect", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_multiselect_field_fail_missing_allow():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(13, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "multiselect", None, None, None, 5,1, { "min_length":0, "max_length":1})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("multiselect", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_multiselect_field_fail_relatedsourcetable():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(13, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "multiselect", None, None, None, 5,None, { "min_length":0,"max_length":1,"allow_empty":False})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("multiselect", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_multiselect_field_fail_relatedsourcefield():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(13, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "multiselect", None, None, None, None,1, { "min_length":0,"max_length":1,"allow_empty":False})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("multiselect", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_multiselect_field_fail_relatedsource_table_and_field():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(13, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "multiselect", None, None, None, None,None, { "min_length":0,"max_length":1,"allow_empty":False})
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("multiselect", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_select_field_relatedsourcetable():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(14, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "select", None, None, None, 5,None)
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("select", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_select_field_relatedsourcefield():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(14, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "select", None, None, None, None,1)
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("select", idd)
    assert r.status_code == 200


@pytest.mark.xfail
def test_add_and_delete_select_field_relatedsource_table_and_field():
    idd = get_id_parameter_table()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    data = data_insert_field(14, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "select", None, None, None, None,None)
    t = add_field(data, idd)
    assert t.status_code == 201
    r = delete_field("select", idd)
    assert r.status_code == 200


def test_delete_parameter_tables():
    idd = get_id_parameter_table()
    dat = json.dumps([
    ])
    r = requests.request("DELETE", url + "parameter-tables/" + str(idd) , headers=header,data=dat)
    assert r.status_code == 200


@pytest.mark.skip
def test_finalize_model():
       data_table = data_parameter_tables(True, True, True, True, True, "hellio", "hfjk", "fffinalizetable", True)
       post_table = add_parameter_table(data_table)
       assert post_table.status_code == 201
       number_table = get_id_parameter_tablee("fffinalizetable")
       dat = data_insert_field(6, "hhhhhyyhhh", "hhhhffhhyyhhh", "jjjffjii", False, "decimal", None, None, None, None,
                                None, {"max_digits": 9, "decimal_places": 7})
       t = add_field(dat, number_table)
       datt = json.dumps(data())
       print(dat)
       r = requests.request("PUT", url + "parameter-tables/" + str(number_table)+"/finalize", headers=header, data=datt)
       assert r.status_code == 200







