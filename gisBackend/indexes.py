import pytest
import requests
import json
from GIS.Gis.parametertables import getall
from GIS.Gis.loginn import url
from GIS.Gis.loginn import header
from GIS.Gis.fields import get_all_fields_by_id_parameter_tables_auxiliary


def get_all_unique_indexes(idd):
    dat = json.dumps({
    })
    r = requests.request("GET", url + "parameter-tables/"+str(idd)+"/indexes", headers=header, data=dat)
    t = r.json()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 200
    listt = []
    for j in range(len(t["content"])):
        listt.append(t["content"][j]["id"])
    return listt


def post_index(number_table):
    listt = get_all_fields_by_id_parameter_tables_auxiliary(number_table)
    print(listt)
    if len(listt) >= 1:
        dat = json.dumps([
            listt[0]
        ])
    r = requests.request("POST", url + "parameter-tables/" + str(number_table) + "/indexes", headers=header, data=dat)


def post_index_xfail(number_table):
    dat = json.dumps([

    ])
    r = requests.request("POST", url + "parameter-tables/" + str(number_table) + "/indexes", headers=header, data=dat)

def get_all_unique_indexes_j(idd):
    dat = json.dumps({
    })
    r = requests.request("GET", url + "parameter-tables/"+str(idd)+"/indexes", headers=header, data=dat)
    t = r.json()
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 200
    return t