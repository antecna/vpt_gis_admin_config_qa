import pytest
import requests
import json
from GIS.Gis.loginn import url
from GIS.Gis.loginn import header
from GIS.Gis.loginn import headers



def data():
    data={

        }
    return data


def getall():
    params = {'page': 0,
              'size': 300}
    r = requests.request("GET", url+"parameter-tables", headers=header, params=params)
    t = r.json()
    return t


def get_id_parameter_table():

   m=getall()
   for i in range(len(m["content"])):
      if m["content"][i]["name"]=="cecina_tabelica":
         ime=m["content"][i]["id"]
   return ime


def get_id_parameter_tablee(name):
    m = getall()
    for i in range(len(m["content"])):
        if m["content"][i]["name"] == name:
            ime = m["content"][i]["id"]
    return ime





def get_ids_parameter_table():
    m = getall()
    list_of_ids = []
    for i in range(len(m["content"])):
        list_of_ids.append(m["content"][i]["id"])
    return list_of_ids


def data_parameter_tables(allowcreate, allowdelete, allowimport, allowintegration, allowupdate, description, label, name, system):
    data = {
        "allowCreate": allowcreate,
        "allowDelete": allowdelete,
        "allowImport":  allowimport,
        "allowIntegration": allowintegration,
        "allowUpdate": allowupdate,
        "description":description,
        "label": label,
        "name":  name,
        "system": system
    }
    return data






def data_parameter_tables_false(allowcreate, allowdelete,allowimport,allowintegration,allowupdate,description,system):
    data = {
        "allowCreate": allowcreate,
        "allowDelete": allowdelete,
        "allowImport": allowimport,
        "allowIntegration": allowintegration,
        "allowUpdate": allowupdate,
        "description": description,
        "system": system
    }
    return data

def number_name(name):
    m = getall()
    k = 0
    for i in range(len(m["content"])):

        if   m["content"][i]["name"] == name :
            k = k + 1
            return k


def add_parameter_table(data):
    dat = json.dumps(data)
    r = requests.request("POST", url + "parameter-tables", headers=header, data=dat)
    return r



def delete_parameter_tables(number_table):
    dataa = json.dumps([
    ])
    r = requests.request("DELETE", url + "parameter-tables/" + str(number_table) , headers=header,data=dataa)
    return r

#print(delete_parameter_tables(75).status_code)
