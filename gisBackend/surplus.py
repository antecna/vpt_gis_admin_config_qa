



'''''
def test_post_insert_index_one():
    idd=get_id_parameter_table()
    listt = get_all_fields_by_id_parameter_tables_auxiliary(idd)
    if len(listt) >= 1:
        dat = json.dumps([
            listt[0]
            ])
    r = requests.request("POST", url + "parameter-tables/"+str(idd)+"/indexes", headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
        assert idd in listi
        assert r.status_code == 201'''


'''def test_post_insert_index_two():
    idd=get_id_parameter_table()
    listt = get_all_fields_by_id_parameter_tables_auxiliary(idd)
    if len(listt) >= 2:
        dat = json.dumps([
                    listt[0],
                    listt[1]
            ])
    r = requests.request("POST", url + "parameter-tables/"+str(idd)+"/indexes", headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 201'''

'''def test_post_insert_index_three():
    idd=get_id_parameter_table()
    listt = get_all_fields_by_id_parameter_tables_auxiliary(idd)
    if len(listt) >= 3:
        dat = json.dumps([
                        listt[0],
                        listt[1],
                        listt[2]
                        ])
    r = requests.request("POST", url + "parameter-tables/"+str(idd)+"/indexes", headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 201'''

'''
def test_post_insert_index_four():
    idd=get_id_parameter_table()
    listt = get_all_fields_by_id_parameter_tables_auxiliary(idd)
    if len(listt) >= 4:
        dat = json.dumps([
                    listt[0],
                    listt[1],
                    listt[2],
                    listt[3]
                        ])
    r = requests.request("POST", url + "parameter-tables/"+str(idd)+"/indexes", headers=header, data=dat)
    m = getall()
    listt = []
    for i in range(len(m["content"])):
        listt.append(m["content"][i]["id"])
    assert idd in listt
    assert r.status_code == 201'''

'''
def test_post_insert_index_five():
    idd=get_id_parameter_table()
    listt = get_all_fields_by_id_parameter_tables_auxiliary(idd)
    if len(listt) >= 5:
        dat = json.dumps([
                        listt[0],
                        listt[1],
                        listt[2],
                        listt[3],
                        listt[4]
                    ])
    r = requests.request("POST", url + "parameter-tables/"+str(idd)+"/indexes", headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 201'''


'''def test_post_insert_index_fail_two():
    idd=get_id_parameter_table()
    listt = get_all_fields_by_id_parameter_tables_auxiliary(idd)
    if len(listt) > 5:
        dat = json.dumps([
                        listt[0],
                        listt[1],
                        listt[2],
                        listt[3],
                        listt[4],
                        listt[5]
                ])
    r = requests.request("POST", url + "parameter-tables/"+str(idd)+"/indexes", headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 201'''

'''
def test_delete_field():
    idd=get_id_parameter_table()
    listt = get_all_fields_by_id_parameter_tables_auxiliary(idd)
    assert len(listt) >= 1

    o = get_all_unique_indexes_j(idd)
    n = get_all_fields_byId_parameter_tables_auxiliary_j(idd)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    for i in range(len(n["content"])):
        if n["content"][i]["id"] == listt[0]:
            name = n["content"][i]["name"]

    listica = []

    for j in range(len(o["content"])):
        if name in n["content"][j]["name"]:
            listica.append(j)
    assert len(listica) >= 1
    if len(listica) >= 1:
        dat = json.dumps([
            ])
        r = requests.request("DELETE", url + "parameter-tables/" + str(idd) + "/fields/" + str(listt[0]), headers=header, data=dat)
        assert r.status_code == 409'''

'''
def test_delete_index():
    idd=get_id_parameter_table()
    listt = get_all_unique_indexes(idd)
    assert len(listt) >= 1
    dat = json.dumps([
        ])
    r = requests.request("DELETE", url + "parameter-tables/" + str(idd) + "/indexes/"+str(listt[0]), headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 200'''

'''@pytest.mark.xfail
def test_delete_index_fail():
    idd=get_id_parameter_table()
    listt = get_all_unique_indexes(idd)
    dat = json.dumps([
        ])
    if len(listt) < 1:
        r = requests.request("DELETE", url + "parameter-tables/" + str(idd) + "/indexes/"+str(listt[0]), headers=header, data=dat)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    assert r.status_code == 200'''


'''
def test_delete_field():
    idd=get_id_parameter_table()
    listt = get_all_fields_by_id_parameter_tables_auxiliary(idd)
    assert len(listt) >= 1

    o = get_all_unique_indexes_j(idd)
    n = get_all_fields_byId_parameter_tables_auxiliary_j(idd)
    m = getall()
    listi = []
    for i in range(len(m["content"])):
        listi.append(m["content"][i]["id"])
    assert idd in listi
    for i in range(len(n["content"])):
        if n["content"][i]["id"] == listt[0]:
            name = n["content"][i]["name"]

    listica = []

    for j in range(len(o["content"])):
        if name in n["content"][j]["name"]:
            listica.append(j)
    assert len(listica) >= 1
    if len(listica) >= 1:
        dat = json.dumps([
            ])
        r = requests.request("DELETE", url + "parameter-tables/" + str(idd) + "/fields/" + str(listt[0]), headers=header, data=dat)
        assert r.status_code == 409'''
