from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By


def click_on_element(driver1, element):
    WebDriverWait(driver1, 5).until(
        EC.visibility_of_element_located((By.CSS_SELECTOR, element)))
    driver1.find_element_by_css_selector(element).click()
    return driver1
    # driver.find_element_by_css_selector(element).click()


def input_values(driver1, element, value):
    WebDriverWait(driver1, 5).until(
        EC.visibility_of_element_located((By.CSS_SELECTOR, element)))
    driver1.find_element_by_css_selector(element).send_keys(value)
    return driver1


def get_text(driver1, element):
    WebDriverWait(driver1, 10).until(
        EC.visibility_of_element_located((By.CSS_SELECTOR, element)))
    return driver1.find_element_by_css_selector(element).text


def get_value(driver1, element):
    WebDriverWait(driver1, 10).until(
        EC.visibility_of_element_located((By.CSS_SELECTOR, element)))
    return driver1.find_element_by_css_selector(element).get_attribute("value")
