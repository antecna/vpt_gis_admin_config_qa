from source import *
import time


def add_field_in_new_table(name, label, helpt):
    f = add_tables("tabs_name", "new")
    time.sleep(1)
    click_on_element(f, Metadata.save_changes_yes)
    f.find_element_by_xpath("//*[@id='1']").click()
    time.sleep(0.5)
    click_on_element(f, ParameterTablesLocators.add_button)
    click_on_element(f, FieldsParameterTable.decimal_field)
    input_values(f, FieldsParameterTable.name, name)
    input_values(f, FieldsParameterTable.label, label)
    input_values(f, FieldsParameterTable.help_text, helpt)
    input_values(f, FieldsParameterTable.describe, "Descrition")
    time.sleep(0.5)
    input_values(f, Decimalfield.digits, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.digits, "10")
    input_values(f, Decimalfield.decimal, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.decimal, "6")
    return f


def add_field(kind, name_field, label, helpt):
    field_selector = FieldsParameterTable.field_type+"(" + kind + ")"
    f = add_tables("tabfin_name", "new")
    time.sleep(1)
    click_on_element(f, Metadata.save_changes_yes)
    # f.find_element_by_xpath("//*[@id='1']").click()
    time.sleep(1)
    click_on_element(f, ParameterTablesLocators.add_button)
    click_on_element(f, field_selector)
    input_values(f, FieldsParameterTable.name, name_field)
    input_values(f, FieldsParameterTable.label, label)
    input_values(f, FieldsParameterTable.help_text, helpt)
    input_values(f, FieldsParameterTable.describe, "Description")
    time.sleep(1)
    return f


def add_decimal_field_in_existing_table(name, label, helpt):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, "ulica")
    click_on_element(f, ParameterTablesLocators.edit)
    time.sleep(1)
    f.find_element_by_xpath("//*[@id='1']").click()
    time.sleep(0.5)
    click_on_element(f, ParameterTablesLocators.add_button)
    click_on_element(f, FieldsParameterTable.decimal_field)
    input_values(f, FieldsParameterTable.name, name)
    input_values(f, FieldsParameterTable.label, label)
    input_values(f, FieldsParameterTable.help_text, helpt)
    input_values(f, FieldsParameterTable.describe, "Descrition")
    time.sleep(0.5)
    input_values(f, Decimalfield.digits, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.digits, "10")
    input_values(f, Decimalfield.decimal, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.decimal, "6")
    return f


def delete_field(driver, name_field):
    click_on_element(driver, FieldsParameterTable.delete)
    time.sleep(0.5)
    click_on_element(driver, FieldsParameterTable.delete_yes)
    time.sleep(1)
    input_values(driver, ParameterTablesLocators.search, name_field)
    message = get_text(driver, FieldsParameterTable.no_data)
    return message


def edit_field(driver, name_field):
    input_values(driver, ParameterTablesLocators.search, name_field)
    click_on_element(driver, ParameterTablesLocators.edit)


def add_field_for_finalization(driver, kind_of_field, name, label, helpt):
    driver.find_element_by_xpath("//*[@id='1']").click()
    time.sleep(0.5)
    click_on_element(driver, ParameterTablesLocators.add_button)
    click_on_element(driver, kind_of_field)
    input_values(driver, FieldsParameterTable.name, name)
    input_values(driver, FieldsParameterTable.label, label)
    input_values(driver, FieldsParameterTable.help_text, helpt)
    input_values(driver, FieldsParameterTable.describe, "Descrition")
    time.sleep(0.5)
