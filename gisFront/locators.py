class LoginPageLocators:
    username = '.ant-input.sc-dlnjwi.sc-jrsJWt.dJXsSm.iJKMLI'
    password = '#root > div > form > div:nth-child(2) > div.sc-bdnxRM.sc-gtsrHT.' \
               'sc-fujyAs.fzUdiI.gfuSqG.eysHZq >span > input'
    signButton = ".ant-btn.ant-btn-primary.sc-crzoAE.gAiKQM"
    labelButton = ".sc-bdnxRM.sc-gtsrHT.sc-jSFjdj.fzUdiI.gfuSqG.bhrCdR"
    labelName = "#root > div > form > div:nth-child(1) > div.sc-bdnxRM.sc-gtsrHT.sc-fujyAs.fzUdiI.gfuSqG.eysHZq > div"
    labelPass = "#root > div > form > div:nth-child(2) > div.sc-bdnxRM.sc-gtsrHT.sc-fujyAs.fzUdiI.gfuSqG.eysHZq > div"


class ModulPageLocators:
    modul_DataModel = ".sc-ksluID.kIzMkM"
    welcome = ".sc-bdnxRM.sc-gtsrHT.sc-bkbkJK.fzUdiI.gfuSqG.bzTlIf"
    language = ".sc-bdnxRM.sc-gtsrHT.sc-hiKfDv.fzUdiI.gfuSqG.jQFBmc"
    logout = ".sc-bdnxRM.sc-gtsrHT.sc-iklJeh.fzUdiI.gfuSqG.gYBAfC"
    icon = ".sc-bdnxRM.sc-gtsrHT.sc-cxNHIi.fzUdiI.gfuSqG.fHUQtx"


class DataModelPageLocators:
    DataModel_Via = ".sc-FRrlG.gPZZZf"
    dataModel_antecna = ".sc-bdnxRM.sc-gtsrHT.sc-bCwfaz.fzUdiI.gfuSqG.fJwrSn.ant-dropdown-trigger"
    pageDataModel = ".sc-bdnxRM.sc-gtsrHT.sc-fXazdy.fzUdiI.gfuSqG.UjHkE > a:nth-child"
    parametarTabel = pageDataModel + "(2)"
    geoobject = pageDataModel + "(3)"
    object = pageDataModel + "(4)"


class ParameterTablesLocators:
    search = ".sc-jNnpgg.jIfccS.css-4cffwv > span > input"
    refresh = ".sc-dPaNzc.fqbJCS.css-4cffwv > button:nth-child(2)"
    add_button = ".ant-btn.sc-carFqZ.fGAvp"
    show_page = ".ant-select-selector"
    page = ".rc-virtual-list > div > div > div > div:nth-child"
    ten_page = page+"(1)"
    twenty_page = page+"(2)"
    fifty_page = page+"(3)"
    one_hundred_page = page+"(4)"
    previous_page = ".ant-pagination-prev"
    next_page = ".ant-pagination-next.ant-pagination-disabled"
    page_number = ".ant-pagination-item.ant-pagination-item-"
    page_number_one = page_number+"1"
    page_number_two = page_number+"2"
    page_number_three = page_number+"3"
    first_element = "tr:nth-child(2) > td:nth-child(1)"
    first_element_only = "tr.ant-table-row.ant-table-row-level-0 > td:nth-child(1) > div"
    noData = ".ant-empty-description"
    edit = "td:nth-child(3) > div > div:nth-child(1) > div > div > div"
    delete = "td:nth-child(3) > div > div:nth-child(6)> div > div > div"
    finished = " td:nth-child(2) > div > div:nth-child(2) > div > div > div"
    columns_and_filters = "td:nth-child(3) > div > div:nth-child(3) > div > div > div"
    create_form = "td:nth-child(3) > div > div:nth-child(4)"
    update_form = "td:nth-child(3) > div > div:nth-child(5)"


class GeoobjectLocators:
    delete_geoobject = "td:nth-child(2) > div > div:nth-child(6) > div > div"
    min_point_count = "#minPointCount"
    max_point_count = "#maxPointCount"
    search = ".sc-jNnpgg.jIfccS.css-4cffwv > span > input"
    edit_geoobject = "td:nth-child(2) > div > div:nth-child(1) > div"
    add_button = ".sc-dPaNzc.fqbJCS.css-4cffwv > button"
    columns_and_filters = "td:nth-child(2) > div > div:nth-child(3) "
    create_form = "td:nth-child(2) > div > div:nth-child(4)"
    edit_form = "td:nth-child(2) > div > div:nth-child(5)"
    min_max_point_count_label = ".ant-form-item-explain.ant-form-item-explain-error"


class Form:
    name = "#name"
    label = "#label"
    description = "#description"
    inline = "#displayType > div:nth-child(1) > label > span:nth-child(2)"
    tabs = "#displayType > div:nth-child(2) > label > span:nth-child(2)"
    next = ".ant-btn.sc-daBunf.iaoaUf"
    add_section = ".ant-btn.sc-carFqZ.fGAvp"
    one_column = "#columnCount > div:nth-child(1) > label > span:nth-child(2)"
    two_column = "#columnCount > div:nth-child(2) > label > span:nth-child(2)"
    next_selection = ".sc-jcsPjo.fNLIsZ.css-4cffwv"
    from_first_place = "div.ant-modal-body > div > div > div > div > div:nth-child(1) > div:nth-child(2) >" \
                       " div:nth-child(1)"
    from_second_place = "div.ant-modal-body > div > div > div > div > div:nth-child(1) > div:nth-child(2) >" \
                        " div:nth-child(2)"
    to_first_place = " div.ant-modal-content > div.ant-modal-body > div > div > div > div > div:nth-child(2) " \
                     "> div:nth-child(2)"
    save = ".ant-btn.sc-eCbnUT.iIjtaC"
    name_selection_text = "tr:nth-child(2) > td:nth-child(2)"
    finalize_first_selection = "div:nth-child(1) > div.sc-eVedaM.bzdSnS.css-4cffwv"
    form_type = ".sc-ojivU.jjQalL.css-4cffwv:nth-child(2)"
    display_type = ".sc-ojivU.jjQalL.css-4cffwv:nth-child(3)"
    edit_section = "tr:nth-child(2) > td:nth-child(7) > div > div"


class Metadata:
    metadata = ".sc-amiJK.eKNisY.css-4cffwv"
    name = "#name"
    label = "#label"
    description = "#description"
    update = "div:nth-child(1) > div:nth-child(3) > div.ant-col.ant-col-16.ant-form-item-control >div>div>label>span"
    create = "div:nth-child(1) > div:nth-child(2) > div.ant-col.ant-col-16.ant-form-item-control >div>div>label>span"
    delete = "div:nth-child(1) > div:nth-child(4) > div.ant-col.ant-col-16.ant-form-item-control >div>div>label>span"
    integration = "div:nth-child(2) > div:nth-child(2)>div.ant-col.ant-col-16.ant-form-item-control >div>div>label>span"
    import_ = " div:nth-child(2) > div:nth-child(3)>div.ant-col.ant-col-16.ant-form-item-control >div>div>label>span"
    system = " div:nth-child(2) > div:nth-child(4)>div.ant-col.ant-col-16.ant-form-item-control  >div>div>label>span"
    cancel = ".sc-kHWWYL.knjike.css-4cffwv"
    next = ".sc-hOPeYd.eXirsP.css-4cffwv"
    save_changes_yes = ".ant-btn.ant-btn-primary"
    save_changes_no = ".ant-modal-confirm-btns > button:nth-child(1)"
    name_label = "div.ant-row.ant-form-item.ant-form-item-with-help.ant-form-item-has-feedback.ant-form-item-has-error"\
                 ">div.ant-col.ant-col-18.ant-form-item-control > div.ant-form-item-explain.ant-form-item-explain-error"
    label_label = "div:nth-child(2)>div.ant-col.ant-col-18.ant-form-item-control>" \
                  "div.ant-form-item-explain.ant-form-item-explain-error"
    helptext_label = "body > div:nth-child(6) > div > div.ant-modal-wrap.ant-modal-centered > div > div.ant-modal-" \
                     "content > div.ant-modal-body > form > div.sc-bQCEYZ.irmCui.css-4cffwv > div:nth-child(1) > " \
                     "div.ant-row.ant-form-item.ant-form-item-with-help.ant-form-item-has-error > " \
                     "div.ant-col.ant-col-18.ant-form-item-control > div.ant-form-item-explain.ant-form" \
                     "-item-explain-error>div"
    geometry_type = ".ant-col.ant-col-15.ant-form-item-control > div > div > div > div"
    point = ".ant-select-item.ant-select-item-option:nth-child(1)"
    multipoint = ".ant-select-item.ant-select-item-option:nth-child(2)"
    line = ".ant-select-item.ant-select-item-option:nth-child(3)"
    multiline = ".ant-select-item.ant-select-item-option:nth-child(4)"
    polygon = ".ant-select-item.ant-select-item-option:nth-child(5)"
    multipolygon = ".ant-select-item.ant-select-item-option:nth-child(6)"


class FieldsParameterTable:
    fields = "#\31"
    field_type = ".ant-modal-body > div:nth-child"
    decimal_field = field_type + "(5)"
    textarea_field = field_type + "(13)"
    select_field = field_type + "(12)"
    autonumber_field = field_type + "(1)"
    boolean_field = field_type + "(2)"
    date_field = field_type + "(3)"
    date_time_field = field_type + "(4)"
    number_field = field_type + "(6)"
    image_field = field_type + "(9)"
    integer_field = field_type + "(7)"
    integer_slider_field = field_type + "(11)"
    char_field = field_type + "(8)"
    time_field = field_type + "(10)"
    first_el_data_type = ".ant-table-row.ant-table-row-level-0 > td:nth-child(3)"
    first_el_name = ".ant-table-row.ant-table-row-level-0 > td:nth-child(1)"
    first_el_edit = ".sc-hTRkXV.qydWF.css-4cffwv"
    edit = "td:nth-child(5) > div > div:nth-child(1)"
    delete = "td:nth-child(5) > div > div:nth-child(2)"
    no_data = ".ant-empty-description"
    delete_yes = ".ant-btn.ant-btn-primary"
    name = "#fieldName"
    label = "#fieldLabel"
    help_text = "#helpText"
    describe = "#defaultWithFormula"
    mandatory = "#mandatory"
    save = "div.sc-dTSzeu.krkYvd.css-4cffwv> button"
    back = ".sc-fmdNqN.hyACbC.css-4cffwv > div.sc-jXcxbT.gLFjH.css-4cffwv"
    cancel = ".ant-modal-close"


class FieldsGeoobject:
    fields = "#\31"
    field_type = ".ant-modal-body > div:nth-child"
    autonumber_field = field_type + "(1)"
    chainage_field = field_type + "(2)"
    boolean_field = field_type + "(3)"
    date_field = field_type + "(4)"
    date_time_field = field_type + "(5)"
    decimal_field = field_type + "(6)"
    file_field = field_type + "(7)"
    number_field = field_type + "(8)"
    image_field = field_type + "(9)"
    integer_field = field_type + "(10)"
    multiselect_field = field_type + "(11)"
    char_field = field_type + "(12)"
    text_field = field_type + "(13)"
    time_field = field_type + "(14)"
    long_lat_alt_field = field_type + "(15)"
    xyz_field = field_type + "(16)"
    integer_slider = field_type + "(17)"
    select_field = field_type + "(18)"
    edit = "td:nth-child(5) > div > div:nth-child(1) > div > div"


class LongLatAltField:
    type = "i div:nth-child(1) > div.ant-col.ant-col-14.ant-form-item-control > div > div > div > div > " \
           "span.ant-select-selection-item"
    position = " div:nth-child(2) > div.ant-col.ant-col-14.ant-form-item-control > div > div > div > div > " \
               "span.ant-select-selection-item"
    lat = "div.ant-select-item:nth-child(2) > div:nth-child(1)"
    centroid = "body > div:nth-child(11) > div > div > div > div.rc-virtual-list > div.rc-virtual-list-holder >" \
               " div > div > div.ant-select-item.ant-select-item-option.ant-select-item-option-active > div"


class XYZField:
    y = "div.ant-select-item:nth-child(2) > div:nth-child(1)"


class Decimalfield:
    digits = "#requiredConstraintsmax_digits"
    decimal = "#requiredConstraintsdecimal_places"


class Textfield:
    max_length = "#requiredConstraintsmax_length"
    max_length_label = ".ant-form-item-explain.ant-form-item-explain-error"


class Selectfield:
    source_table = "div:nth-child(1) > div.ant-col.ant-col-14.ant-form-item-control"
    source_table_value = ".ant-select-item-option-content"
    source_field = "div:nth-child(2) > div.ant-col.ant-col-14.ant-form-item-control"
    source_field_value = "body > div:nth-child(9) > div > div > div > div.rc-virtual-list > div.rc-virtual-list-holder"\
                         " > div > div > div.ant-select-item.ant-select-item-option.ant-select-item-option-active > div"
    defalt_value = "div:nth-child(3) > div.ant-col.ant-col-14.ant-form-item-control"


class Indexes:
    field = ".ant-select.sc-eXuyPJ.cqUdDr.ant-select-single.ant-select-show-arrow"
    field_one = ".ant-select-item-option-content"
    add_new = ".sc-gzcbmu.fKwyEY"
    save = ".sc-eWnToP.bJkVpi.css-4cffwv"
    delete = ".sc-khIgEk.erAmrt.css-4cffwv"
    delete_field_three = "div:nth-child(3) > div.sc-Arkif.dkxMsu.css-4cffwv > div"
    delete_field_four = "div:nth-child(4) > div.sc-Arkif.dkxMsu.css-4cffwv > div"
    delete_field_five = "div:nth-child(5) > div.sc-Arkif.dkxMsu.css-4cffwv > div"


class Finalization:
    finalization = "#\33"
    finalize_button = ".sc-hOPeYd.eXirsP.css-4cffwv > button"
    constrains = "tr.ant-table-row.ant-table-row-level-0 > td:nth-child(4)"


class Autonumberfield:
    numbers_to_display = "#requiredConstraintspadding"
    numbers_to_display_label = "div:nth-child(1) > div > div > div.ant-col.ant-col-14.ant-form-item-control > " \
                               "div.ant-form-item-explain.ant-form-item-explain-error > div"


class Integerslider:
    min_value = "#requiredConstraintsmin_value"
    max_value = "#requiredConstraintsmax_value"
    step = "#requiredConstraintsstep"
    min_value_label = "div:nth-child(1) > div.ant-col.ant-col-14.ant-form-item-control " \
                      "> div.ant-form-item-explain.ant-form-item-explain-error > div"
    max_value_label = "div:nth-child(2) > div.ant-col.ant-col-14.ant-form-item-control " \
                      "> div.ant-form-item-explain.ant-form-item-explain-error > div"
    step_label = "div:nth-child(3) > div.ant-col.ant-col-14.ant-form-item-control " \
                 "> div.ant-form-item-explain.ant-form-item-explain-error > div"


class Multiselectfield:
    domaintype = "div:nth-child(1) > div > div:nth-child(1) > div.ant-col.ant-col-14.ant-form-item-control > div"
    domaintype_value = "div.ant-select-item.ant-select-item-option.ant-select-item-option-active > div"
    source_table = "div:nth-child(1) > div > div:nth-child(2) > div.ant-col.ant-col-14.ant-form-item-control > div"
    source_table_value = "body > div:nth-child(12) > div > div > div > div.rc-virtual-list > " \
                         "div.rc-virtual-list-holder > div > div > div:nth-child(1) > div"
    source_field = "div:nth-child(1) > div > div:nth-child(3) > div.ant-col.ant-col-14.ant-form-item-control > div"
    source_field_value = "body > div:nth-child(13) > div > div > div > div.rc-virtual-list > " \
                         "div.rc-virtual-list-holder > div > div > div.ant-select-item.ant-select-item-option" \
                         ".ant-select-item-option-active > div"
    defalt_value = "div:nth-child(3) > div.ant-col.ant-col-14.ant-form-item-control"
    minimum_number = "#requiredConstraintsmin_length"
    maximum_number = "#requiredConstraintsmax_length"
    allow_empty = "#requiredConstraintsallow_empty"
    minimum_number_label = "div:nth-child(1) > div.ant-col.ant-col-14.ant-form-item-control > " \
                           "div.ant-form-item-explain.ant-form-item-explain-error > div"
    maximum_number_label = "div:nth-child(2) > div.ant-col.ant-col-14.ant-form-item-control > " \
                           "div.ant-form-item-explain.ant-form-item-explain-error > div"
    allow_empty_label = "div:nth-child(3) > div.ant-col.ant-col-14.ant-form-item-control > " \
                        "div.ant-form-item-explain.ant-form-item-explain-error > div"


class Charfield:
    max_length = "#requiredConstraintsmax_length"
    max_length_label = ".ant-form-item-explain.ant-form-item-explain-error"


class ColumnsFilters:
    labels_from_first_place = "div.sc-bUQyIj.eDAVHd > div > div > div:nth-child(1)"
    labels_from_second = "div.sc-bUQyIj.eDAVHd > div > div > div:nth-child(2)"
    labels_to_first_place = ".sc-hYRTwp.iRaesw > div > div > div:nth-child(1)"
    labels_to_central_place = ".sc-dksuTV.zPuL"
    labels_to_second_place = ".sc-hYRTwp.iRaesw > div > div > div:nth-child(2)"
    labels_to_third_place = ".sc-hYRTwp.iRaesw > div > div > div:nth-child(3)"
    next_finalize_button = ".sc-hOPeYd.eXirsP.css-4cffwv > button"
    view_filters_first = "div.sc-hYIrvc.daHyTD.css-4cffwv > div:nth-child(1)"
    view_filters_second = "div.sc-hYIrvc.daHyTD.css-4cffwv > div:nth-child(2)"
    view_columns_first = ".sc-kHVIJG.jNudlt.css-4cffwv > div:nth-child(1) > div:nth-child(2)"
    view_columns_second = ".sc-kHVIJG.jNudlt.css-4cffwv > div:nth-child(1) > div:nth-child(3)"
