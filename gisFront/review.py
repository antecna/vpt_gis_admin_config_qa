import config
from selenium import webdriver
import pytest
from locators import *
import time


@pytest.fixture(params=['chrome', 'firefox'])
def testing(request):
    word = "tabela_prikaz"
    param = request.param
    if param == "firefox":
        driver = webdriver.Firefox(executable_path=r'C:\Users\Korisnik\PycharmProjects\pythonProject'
                                                   r'\seleniumTest\browser\mozlia_driver\geckodriver.exe')
    else:
        if param == "chrome":
            driver = webdriver.Chrome(r'C:\Users\Korisnik\PycharmProjects\pythonProject\seleniumTest'
                                      r'\browser\chromedriver.exe')
    driver.maximize_window()
    time.sleep(1)
    driver.get(config.url)
    driver.find_element_by_css_selector(LoginPageLocators.username).send_keys("admin")
    driver.find_element_by_css_selector(LoginPageLocators.password).send_keys("admin")
    driver.find_element_by_css_selector(LoginPageLocators.signButton).click()
    time.sleep(1)
    driver.find_element_by_css_selector(ModulPageLocators.modul_DataModel).click()
    driver.find_element_by_css_selector(DataModelPageLocators.parametarTabel).click()
    time.sleep(1)
    driver.find_element_by_css_selector(ParameterTablesLocators.add_button).click()
    time.sleep(1)
    driver.implicitly_wait(5)
    driver.find_element_by_css_selector(Metadata.name).send_keys(word)
    driver.find_element_by_css_selector(Metadata.label).send_keys("label_oneee")
    driver.find_element_by_css_selector(Metadata.description).send_keys("Desription...")
    driver.find_element_by_css_selector(Metadata.create).click()
    driver.find_element_by_css_selector(Metadata.delete).click()
    driver.find_element_by_css_selector(Metadata.system).click()
    driver.find_element_by_css_selector(Metadata.import_).click()
    driver.find_element_by_css_selector(Metadata.next).click()
    time.sleep(1)
    driver.find_element_by_css_selector(Metadata.next)
    driver.find_element_by_css_selector(Metadata.save_changes_yes).click()
    time.sleep(1)
    driver.find_element_by_css_selector(DataModelPageLocators.parametarTabel).click()
    time.sleep(1)
    driver.find_element_by_css_selector(ParameterTablesLocators.search).send_keys(word)
    time.sleep(1)
    element = driver.find_element_by_css_selector(ParameterTablesLocators.first_element_only).text
    time.sleep(1)
    driver.find_element_by_css_selector(ParameterTablesLocators.edit).click()
    driver.find_element_by_css_selector(Metadata.metadata).click()
    time.sleep(1)
    description = driver.find_element_by_css_selector(Metadata.description).text
    time.sleep(1)
    driver.find_element_by_css_selector(Metadata.description).send_keys(" Dodatak")
    time.sleep(1)
    driver.find_element_by_css_selector(Metadata.next).click()
    time.sleep(1)
    driver.find_element_by_css_selector(Metadata.save_changes_yes).click()
    time.sleep(1)
    driver.find_element_by_css_selector(DataModelPageLocators.parametarTabel).click()
    time.sleep(1)
    driver.find_element_by_css_selector(ParameterTablesLocators.search).send_keys(word)
    time.sleep(1)
    driver.find_element_by_css_selector(ParameterTablesLocators.edit).click()
    time.sleep(1)
    driver.find_element_by_css_selector(Metadata.metadata).click()
    time.sleep(1)
    description_new = driver.find_element_by_css_selector(Metadata.description).text
    driver.close()
    return word, element, description_new, description


def test(testing):
    assert testing[0] == testing[1]
    assert testing[3] + " Dodatak" in testing[2]
