import config
from selenium import webdriver
from locators import *
import time
from basepage import *
from fields import *


def test_0():
    word = "antecna"
    driver = webdriver.Chrome(r'C:\Users\Korisnik\PycharmProjects\pythonProject\seleniumTest\browser\chromedriver.exe')
    driver.maximize_window()
    time.sleep(1)
    driver.get(config.url)
    input_values(driver, LoginPageLocators.username, "admin")
    input_values(driver, LoginPageLocators.password, "admin")
    click_on_element(driver, LoginPageLocators.signButton)
    time.sleep(1)
    click_on_element(driver, ModulPageLocators.modul_DataModel)
    click_on_element(driver, DataModelPageLocators.parametarTabel)
    click_on_element(driver, ParameterTablesLocators.add_button)
    input_values(driver, Metadata.name, word)
    input_values(driver, Metadata.label, "labelone")
    input_values(driver, Metadata.description, "description")
    click_on_element(driver, Metadata.create)
    click_on_element(driver, Metadata.delete)
    click_on_element(driver, Metadata.system)
    click_on_element(driver, Metadata.import_)
    click_on_element(driver, Metadata.next)
    click_on_element(driver, Metadata.save_changes_yes)
    click_on_element(driver, ParameterTablesLocators.add_button)
    click_on_element(driver, FieldsParameterTable.autonumber_field)
    name_autonumber = "autonumber"
    input_values(driver, FieldsParameterTable.name, name_autonumber)
    input_values(driver, FieldsParameterTable.label, "label")
    input_values(driver, FieldsParameterTable.help_text, "helpp")
    input_values(driver, Autonumberfield.numbers_to_display, "5")
    time.sleep(1)
    click_on_element(driver, FieldsParameterTable.save)
    input_values(driver, ParameterTablesLocators.search, "autonumber")
    typee = get_text(driver, FieldsParameterTable.first_el_data_type)
    name = get_text(driver, FieldsParameterTable.first_el_name)
    assert typee == "autonumber"
    assert name == "autonumber"
    time.sleep(1)
    click_on_element(driver, FieldsParameterTable.edit)
    input_values(driver, Autonumberfield.numbers_to_display, u'\ue009' + u'\ue003')
    input_values(driver, Autonumberfield.numbers_to_display, "10")
    click_on_element(driver, FieldsParameterTable.save)
    input_values(driver, ParameterTablesLocators.search, "autonumber")
    click_on_element(driver, FieldsParameterTable.edit)
    padding = get_value(driver, Autonumberfield.numbers_to_display)
    assert padding == "10"
    click_on_element(driver, FieldsParameterTable.cancel)
    time.sleep(2)
    input_values(driver, ParameterTablesLocators.search, "autonumber")
    message = delete_field(driver, "autonumber")
    assert message == "No Data"
    click_on_element(driver, ParameterTablesLocators.add_button)
    click_on_element(driver, FieldsParameterTable.autonumber_field)
    name_autonumber = "autonumber"
    input_values(driver, FieldsParameterTable.name, name_autonumber)
    input_values(driver, FieldsParameterTable.label, "label")
    input_values(driver, FieldsParameterTable.help_text, "helpp")
    input_values(driver, Autonumberfield.numbers_to_display, "5")
    time.sleep(1)
    click_on_element(driver, FieldsParameterTable.save)
    click_on_element(driver, ParameterTablesLocators.add_button)
    click_on_element(driver, FieldsParameterTable.integer_field)
    name_slider = "integer_slider"
    input_values(driver, FieldsParameterTable.name, name_slider)
    input_values(driver, FieldsParameterTable.label, "label")
    input_values(driver, FieldsParameterTable.help_text, "helpp")
    input_values(driver, Integerslider.min_value, u'\ue009' + u'\ue003')
    input_values(driver, Integerslider.min_value, "2")
    input_values(driver, Integerslider.max_value, u'\ue009' + u'\ue003')
    input_values(driver, Integerslider.max_value, "95")
    input_values(driver, Integerslider.step, u'\ue009' + u'\ue003')
    input_values(driver, Integerslider.step, "3")
    click_on_element(driver, FieldsParameterTable.save)
    click_on_element(driver, ParameterTablesLocators.add_button)
    click_on_element(driver, FieldsParameterTable.char_field)
    name_char = "char"
    input_values(driver, FieldsParameterTable.name, name_char)
    input_values(driver, FieldsParameterTable.label, "label")
    input_values(driver, FieldsParameterTable.help_text, "helpp")
    input_values(driver, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(driver, Charfield.max_length, "76")
    click_on_element(driver, FieldsParameterTable.save)
    time.sleep(1)
    driver.find_element_by_xpath("//*[@id='2']").click()
    click_on_element(driver, ParameterTablesLocators.add_button)
    time.sleep(0.5)
    lis = []
    for i in range(2):
        click_on_element(driver, Indexes.add_new)
    e = driver.find_elements_by_css_selector(Indexes.field)
    for j in range(3):
        e[j].click()
        time.sleep(0.5)
        p = driver.find_elements_by_css_selector(Indexes.field_one)
        p[j].click()
        lis.append(p[j].text)
    driver.find_element_by_css_selector(Indexes.save).click()
    time.sleep(2)
    driver.find_element_by_xpath("//*[@id='3']").click()
    constarins = get_text(driver, Finalization.constrains)
    assert constarins == "max_length = 76"
    click_on_element(driver, Finalization.finalize_button)
    click_on_element(driver, Metadata.save_changes_yes)
    click_on_element(driver, DataModelPageLocators.parametarTabel)
    input_values(driver, ParameterTablesLocators.search, word)
    time.sleep(1)
    finished = driver.find_element_by_css_selector(ParameterTablesLocators.finished).is_enabled()
    assert finished is True
    driver.close()
