from selenium import webdriver
from locators import *
import config
import time
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.opera import OperaDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from basepage import *
from selenium.webdriver.common.action_chains import ActionChains


def login(name="admin@planetsoft.eu", password="bonus"):
    # driver = webdriver.Chrome(r'C:\Users\Korisnik\PycharmProjects\pythonProject\seleniumTest\browser\chromedriver.exe')
    # driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())

    driver = webdriver.Firefox(
    executable_path=r'C:\Users\Korisnik\PycharmProjects\pythonProject\seleniumTest\browser\mozlia_driver\geckodriver.exe',
    )
    # driver = webdriver.Opera(
    # executable_path=r'C:\Users\Korisnik\PycharmProjects\pythonProject\seleniumTest\browser\mozlia_driver\operadriver.exe',
    # )
    # driver = webdriver.Opera(executable_path=OperaDriverManager().install())
    # driver = webdriver.Edge(executable_path=EdgeChromiumDriverManager().install())
    driver.maximize_window()
    driver.get(config.url)
    input_values(driver, LoginPageLocators.username, name)
    input_values(driver, LoginPageLocators.password, password)
    click_on_element(driver, LoginPageLocators.signButton)
    time.sleep(1)
    return driver


def lang():
    f = login()
    click_on_element(f, ModulPageLocators.icon)
    time.sleep(1)
    click_on_element(f, ModulPageLocators.language)
    return f


def add_tables(name, label):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    click_on_element(f, ParameterTablesLocators.add_button)
    input_values(f, Metadata.name, name)
    input_values(f, Metadata.label, label)
    input_values(f, Metadata.description, "Description...")
    click_on_element(f, Metadata.create)
    click_on_element(f, Metadata.delete)
    click_on_element(f, Metadata.system)
    click_on_element(f, Metadata.import_)
    click_on_element(f, Metadata.next)
    return f


def delete_table(driver, name_table):
    time.sleep(1)
    click_on_element(driver, DataModelPageLocators.parametarTabel)
    input_values(driver, ParameterTablesLocators.search, name_table)
    time.sleep(1)
    click_on_element(driver, ParameterTablesLocators.delete)
    time.sleep(1)
    click_on_element(driver, Metadata.save_changes_yes)
    time.sleep(1)


def add_index(name_table):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, name_table)
    time.sleep(1)
    click_on_element(f, ParameterTablesLocators.edit)
    time.sleep(1)
    f.find_element_by_xpath("//*[@id='2']").click()
    click_on_element(f, ParameterTablesLocators.add_button)
    time.sleep(0.5)
    list_field = []
    for i in range(4):
        click_on_element(f, Indexes.add_new)
    elements_field = f.find_elements_by_css_selector(Indexes.field)
    for j in range(5):
        elements_field[j].click()
        time.sleep(1)
        dropdown = (By.CSS_SELECTOR, 'div[class*="ant-select-dropdown"] .rc-virtual-list')  # div-ove izdvoji
        all_dropdowns = f.find_elements(*dropdown)
        time.sleep(1)
        list_field.append(all_dropdowns[j].find_element_by_css_selector('.ant-select-item.ant-select-item-option').text)
        all_dropdowns[j].find_element_by_css_selector('.ant-select-item.ant-select-item-option').click()
    r = name_table + "__" + list_field[0] + "_" + list_field[1]
    return f, r


def delete_index(driv, r):
    input_values(driv, ParameterTablesLocators.search, r)
    time.sleep(1)
    click_on_element(driv, Indexes.delete)
    time.sleep(1)
    click_on_element(driv, FieldsParameterTable.delete_yes)
    input_values(driv, ParameterTablesLocators.search, r)
    time.sleep(1)
    return driv


def create_form_table(name_table):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, name_table)
    click_on_element(f, ParameterTablesLocators.create_form)
    input_values(f, Form.name, "name_form")
    input_values(f, Form.label, "label")
    input_values(f, Form.description, "description")
    time.sleep(1)
    click_on_element(f, Form.inline)
    click_on_element(f, Form.next)
    time.sleep(1)
    for counter in range(2):
        click_on_element(f, Form.add_section)
        if counter == 0:
            input_values(f, Form.name, "section_name1")
        else:
            input_values(f, Form.name, "section_name2")
        input_values(f, Form.label, "label")
        input_values(f, Form.description, "description")
        click_on_element(f, Form.one_column)
        time.sleep(1)
        elements = f.find_elements_by_css_selector(Form.next_selection)
        elements[1].click()
        for i in range(2):
            source = f.find_element_by_css_selector(Form.from_first_place)
            target = f.find_element_by_css_selector(Form.to_first_place)
            ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        click_on_element(f, Form.save)
    message_first_section = get_text(f, Form.name_selection_text)
    click_on_element(f, Form.next)
    message_first_section_finalize = get_text(f, Form.finalize_first_selection)
    display_type = get_text(f, Form.display_type)
    form_type = get_text(f, Form.form_type)
    #  click_on_element(f, Finalization.finalize_button)
    return f, message_first_section, message_first_section_finalize, display_type, form_type


def update_form_table(name_table):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, name_table)
    click_on_element(f, ParameterTablesLocators.update_form)
    input_values(f, Form.name, "name_form")
    input_values(f, Form.label, "label")
    input_values(f, Form.description, "description")
    time.sleep(1)
    click_on_element(f, Form.inline)
    click_on_element(f, Form.next)
    time.sleep(1)
    for counter in range(2):
        click_on_element(f, Form.add_section)
        if counter == 0:
            input_values(f, Form.name, "section_name1")
        else:
            input_values(f, Form.name, "section_name2")
        input_values(f, Form.label, "label")
        input_values(f, Form.description, "description")
        click_on_element(f, Form.one_column)
        time.sleep(1)
        elements = f.find_elements_by_css_selector(Form.next_selection)
        elements[1].click()
        for i in range(2):
            source = f.find_element_by_css_selector(Form.from_first_place)
            target = f.find_element_by_css_selector(Form.to_first_place)
            ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        click_on_element(f, Form.save)
    message_first_section = get_text(f, Form.name_selection_text)
    click_on_element(f, Form.next)
    message_first_section_finalze = get_text(f, Form.finalize_first_selection)
    display_type = get_text(f, Form.display_type)
    form_type = get_text(f, Form.form_type)
    #  click_on_element(f, Finalization.finalize_button)
    return f, message_first_section, message_first_section_finalze, display_type, form_type
