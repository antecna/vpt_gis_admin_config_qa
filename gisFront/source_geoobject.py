from selenium import webdriver
from locators import *
import config
import time
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.opera import OperaDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from basepage import *
from source import login
from selenium.webdriver.common.action_chains import ActionChains


def add_geoobject(name, label, geometry_type_value):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.geoobject)
    click_on_element(f, ParameterTablesLocators.add_button)
    input_values(f, Metadata.name, name)
    input_values(f, Metadata.label, label)
    input_values(f, Metadata.description, "Description...")
    click_on_element(f, Metadata.create)
    click_on_element(f, Metadata.delete)
    click_on_element(f, Metadata.import_)
    click_on_element(f, Metadata.geometry_type)
    time.sleep(1)
    f.find_element_by_css_selector(".ant-select-item.ant-select-item-option:nth-child(" + geometry_type_value + ")")\
        .click()
    return f


def delete_geoobject(driver, name_geoobject):
    time.sleep(1)
    click_on_element(driver, DataModelPageLocators.geoobject)
    input_values(driver, ParameterTablesLocators.search, name_geoobject)
    time.sleep(1)
    click_on_element(driver, GeoobjectLocators.delete_geoobject)
    time.sleep(1)
    click_on_element(driver, Metadata.save_changes_yes)
    time.sleep(1)


def add_multiselect_field_in_new_geoobject(name, label, helpt):
    f = add_geoobject("geo_name", "new", "1")
    time.sleep(1)
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    time.sleep(1)
    click_on_element(f, GeoobjectLocators.add_button)
    click_on_element(f, FieldsGeoobject.multiselect_field)
    input_values(f, FieldsParameterTable.name, name)
    input_values(f, FieldsParameterTable.label, label)
    input_values(f, FieldsParameterTable.help_text, helpt)
    input_values(f, FieldsParameterTable.describe, "Descrition")
    time.sleep(0.5)
    return f


def delete_field_in_geoobject(driver, name_field):
    click_on_element(driver, FieldsParameterTable.delete)
    time.sleep(0.5)
    click_on_element(driver, FieldsParameterTable.delete_yes)
    time.sleep(1)
    input_values(driver, ParameterTablesLocators.search, name_field)
    message = get_text(driver, FieldsParameterTable.no_data)
    return message


def add_field_in_geoobject(kind, name_field, label, helpt):
    field_selector = FieldsParameterTable.field_type+"(" + kind + ")"
    f = add_geoobject("s788", "new", "1")
    time.sleep(1)
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    time.sleep(1)
    click_on_element(f, GeoobjectLocators.add_button)
    click_on_element(f, field_selector)
    input_values(f, FieldsParameterTable.name, name_field)
    input_values(f, FieldsParameterTable.label, label)
    input_values(f, FieldsParameterTable.help_text, helpt)
    input_values(f, FieldsParameterTable.describe, "Description")
    time.sleep(1)
    return f


def add_field_for_finalization(driver, kind_of_field, name, label, helpt):
    driver.find_element_by_xpath("//*[@id='1']").click()
    time.sleep(0.5)
    click_on_element(driver, GeoobjectLocators.add_button)
    click_on_element(driver, kind_of_field)
    input_values(driver, FieldsParameterTable.name, name)
    input_values(driver, FieldsParameterTable.label, label)
    input_values(driver, FieldsParameterTable.help_text, helpt)
    input_values(driver, FieldsParameterTable.describe, "Descrition")
    time.sleep(0.5)


def add_index_in_geoobject(name_geoobject):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.geoobject)
    input_values(f, ParameterTablesLocators.search, name_geoobject)
    time.sleep(1)
    click_on_element(f, GeoobjectLocators.edit_geoobject)
    time.sleep(1)
    f.find_element_by_xpath("//*[@id='2']").click()
    click_on_element(f, ParameterTablesLocators.add_button)
    time.sleep(0.5)
    list_field = []
    for i in range(4):
        click_on_element(f, Indexes.add_new)
    elements_field = f.find_elements_by_css_selector(Indexes.field)
    for j in range(5):
        elements_field[j].click()
        time.sleep(1)
        dropdown = (By.CSS_SELECTOR, 'div[class*="ant-select-dropdown"] .rc-virtual-list')  # div-ove izdvoji
        all_dropdowns = f.find_elements(*dropdown)
        time.sleep(1)
        list_field.append(all_dropdowns[j].find_element_by_css_selector('.ant-select-item.ant-select-item-option').text)
        all_dropdowns[j].find_element_by_css_selector('.ant-select-item.ant-select-item-option').click()
    name_index = name_geoobject + "__" + list_field[0] + "_" + list_field[1]
    return f, name_index


def delete_index_in_geoobject(driv, r):
    input_values(driv, ParameterTablesLocators.search, r)
    time.sleep(1)
    click_on_element(driv, Indexes.delete)
    time.sleep(1)
    click_on_element(driv, FieldsParameterTable.delete_yes)
    input_values(driv, ParameterTablesLocators.search, r)
    time.sleep(1)
    return driv


def update_form(name_geoobject):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.geoobject)
    input_values(f, ParameterTablesLocators.search, name_geoobject)
    click_on_element(f, GeoobjectLocators.edit_form)
    input_values(f, Form.name, "name_form")
    input_values(f, Form.label, "label")
    input_values(f, Form.description, "description")
    time.sleep(1)
    click_on_element(f, Form.inline)
    click_on_element(f, Form.next)
    time.sleep(1)
    for counter in range(2):
        click_on_element(f, Form.add_section)
        if counter == 0:
            input_values(f, Form.name, "section_name1")
        else:
            input_values(f, Form.name, "section_name2")
        input_values(f, Form.label, "label")
        input_values(f, Form.description, "description")
        click_on_element(f, Form.one_column)
        time.sleep(1)
        elements = f.find_elements_by_css_selector(Form.next_selection)
        elements[1].click()
        for i in range(2):
            source = f.find_element_by_css_selector(Form.from_first_place)
            target = f.find_element_by_css_selector(Form.to_first_place)
            ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        click_on_element(f, Form.save)
    message_first_section = get_text(f, Form.name_selection_text)
    click_on_element(f, Form.next)
    message_first_section_finalze = get_text(f, Form.finalize_first_selection)
    display_type = get_text(f, Form.display_type)
    form_type = get_text(f, Form.form_type)
    #  click_on_element(f, Finalization.finalize_button)
    return f, message_first_section, message_first_section_finalze, display_type, form_type


def create_form(name_geoobject):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.geoobject)
    input_values(f, ParameterTablesLocators.search, name_geoobject)
    time.sleep(1)
    click_on_element(f, GeoobjectLocators.create_form)
    input_values(f, Form.name, "name_form")
    input_values(f, Form.label, "label")
    input_values(f, Form.description, "description")
    time.sleep(1)
    click_on_element(f, Form.tabs)
    click_on_element(f, Form.next)
    time.sleep(1)
    for counter in range(2):
        click_on_element(f, Form.add_section)
        if counter == 0:
            input_values(f, Form.name, "section_name1")
        else:
            input_values(f, Form.name, "section_name2")
        input_values(f, Form.label, "label")
        input_values(f, Form.description, "description")
        click_on_element(f, Form.one_column)
        time.sleep(1)
        elements = f.find_elements_by_css_selector(Form.next_selection)
        elements[1].click()
        for i in range(2):
            source = f.find_element_by_css_selector(Form.from_first_place)
            target = f.find_element_by_css_selector(Form.to_first_place)
            ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        click_on_element(f, Form.save)
    message_first_section = get_text(f, Form.name_selection_text)
    click_on_element(f, Form.next)
    message_first_section_finalize = get_text(f, Form.finalize_first_selection)
    display_type = get_text(f, Form.display_type)
    form_type = get_text(f, Form.form_type)
    #  click_on_element(f, Finalization.finalize_button)
    return f, message_first_section, message_first_section_finalize, display_type, form_type


def edit_update_form(name_geoobject):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.geoobject)
    input_values(f, ParameterTablesLocators.search, name_geoobject)
    time.sleep(1)
    click_on_element(f, GeoobjectLocators.edit_form)
    time.sleep(1)
    input_values(f, Form.name,  u'\ue009' + u'\ue003')
    input_values(f, Form.name, "name_form")
    input_values(f, Form.label, "label")
    input_values(f, Form.description, "description")
    time.sleep(1)
    click_on_element(f, Form.tabs)
    click_on_element(f, Form.next)
    time.sleep(1)
    click_on_element(f, Form.edit_section)
    input_values(f, Form.name,  u'\ue009' + u'\ue003')
    input_values(f, Form.name, "section_name1_new")
    click_on_element(f, Form.one_column)
    elements = f.find_elements_by_css_selector(Form.next_selection)
    elements[1].click()
    time.sleep(0.5)
    click_on_element(f, Form.save)
    message_first_section = get_text(f, Form.name_selection_text)
    click_on_element(f, Form.next)
    display_type = get_text(f, Form.display_type)
    form_type = get_text(f, Form.form_type)
    #  click_on_element(f, Finalization.finalize_button)
    return f, message_first_section, display_type, form_type


def edit_create_form(name_geoobject):
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.geoobject)
    input_values(f, ParameterTablesLocators.search, name_geoobject)
    time.sleep(1)
    click_on_element(f, GeoobjectLocators.create_form)
    time.sleep(1)
    input_values(f, Form.name,  u'\ue009' + u'\ue003')
    input_values(f, Form.name, "name_form")
    input_values(f, Form.label, "label")
    input_values(f, Form.description, "description")
    time.sleep(1)
    click_on_element(f, Form.inline)
    click_on_element(f, Form.next)
    time.sleep(1)
    click_on_element(f, Form.edit_section)
    input_values(f, Form.name,  u'\ue009' + u'\ue003')
    input_values(f, Form.name, "section_name1_new")
    click_on_element(f, Form.one_column)
    elements = f.find_elements_by_css_selector(Form.next_selection)
    elements[1].click()
    time.sleep(0.5)
    click_on_element(f, Form.save)
    message_first_section = get_text(f, Form.name_selection_text)
    click_on_element(f, Form.next)
    display_type = get_text(f, Form.display_type)
    form_type = get_text(f, Form.form_type)
    #  click_on_element(f, Finalization.finalize_button)
    return f, message_first_section, display_type, form_type
