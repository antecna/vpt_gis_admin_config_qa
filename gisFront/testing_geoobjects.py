from source_geoobject import *
import allure
import pytest
import time


@allure.severity(allure.severity_level.NORMAL)
def test_001_add_geoobject_point_correct():
    f = add_geoobject("correct_namee", "_label", "1")
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    delete_geoobject(f, "correct_namee")
    input_values(f, ParameterTablesLocators.search, "correct_namee")
    message = get_text(f, FieldsParameterTable.no_data)
    assert message == "No Data"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_002_add_geoobject_multipoint_correct():
    f = add_geoobject("correct_namee", "_label", "2")
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    delete_geoobject(f, "correct_namee")
    input_values(f, ParameterTablesLocators.search, "correct_namee")
    message = get_text(f, FieldsParameterTable.no_data)
    assert message == "No Data"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_003_add_geoobject_linestring_correct():
    f = add_geoobject("correct_namee", "_label", "3")
    input_values(f, GeoobjectLocators.min_point_count, "2")
    input_values(f, GeoobjectLocators.max_point_count, "4")
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    delete_geoobject(f, "correct_namee")
    input_values(f, ParameterTablesLocators.search, "correct_namee")
    message = get_text(f, FieldsParameterTable.no_data)
    assert message == "No Data"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_004_add_geoobject_multilinestring_correct():
    f = add_geoobject("correct_namee", "_label", "4")
    input_values(f, GeoobjectLocators.min_point_count, "2")
    input_values(f, GeoobjectLocators.max_point_count, "4")
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    delete_geoobject(f, "correct_namee")
    input_values(f, ParameterTablesLocators.search, "correct_namee")
    message = get_text(f, FieldsParameterTable.no_data)
    assert message == "No Data"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_005_add_geoobject_polygon_correct():
    f = add_geoobject("correct_namee", "_label", "5")
    input_values(f, GeoobjectLocators.min_point_count, "3")
    input_values(f, GeoobjectLocators.max_point_count, "4")
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    delete_geoobject(f, "correct_namee")
    input_values(f, ParameterTablesLocators.search, "correct_namee")
    message = get_text(f, FieldsParameterTable.no_data)
    assert message == "No Data"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_006_add_geoobject_multipolygon_correct():
    f = add_geoobject("correct_namee", "_label", "6")
    input_values(f, GeoobjectLocators.min_point_count, "3")
    input_values(f, GeoobjectLocators.max_point_count, "4")
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    delete_geoobject(f, "correct_namee")
    input_values(f, ParameterTablesLocators.search, "correct_namee")
    message = get_text(f, FieldsParameterTable.no_data)
    assert message == "No Data"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_007_edit_geoobject_polygon():
    f = add_geoobject("correct_namee", "_label", "5")
    input_values(f, GeoobjectLocators.min_point_count, "3")
    input_values(f, GeoobjectLocators.max_point_count, "4")
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    click_on_element(f, DataModelPageLocators.geoobject)
    input_values(f, GeoobjectLocators.search, "correct_namee")
    click_on_element(f, GeoobjectLocators.edit_geoobject)
    time.sleep(1)
    f.find_element_by_xpath("//*[@id='0']").click()
    input_values(f, Metadata.description, u'\ue009' + u'\ue003')
    input_values(f, Metadata.description, "Dodaj neki opis")
    click_on_element(f, Metadata.next)
    time.sleep(1)
    click_on_element(f, Metadata.save_changes_yes)
    time.sleep(1)
    click_on_element(f, DataModelPageLocators.geoobject)
    input_values(f, ParameterTablesLocators.search, "correct_namee")
    click_on_element(f, GeoobjectLocators.edit_geoobject)
    f.find_element_by_xpath("//*[@id='0']").click()
    message = get_text(f, Metadata.description)
    assert "Dodaj neki opis" in message
    delete_geoobject(f, "correct_namee")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_008_add_geoobject_with_incorrect_name():
    f = add_geoobject("incorrec1/*", "_label", "1")
    message = get_text(f, Metadata.name_label)
    assert message == "Name must contain only lower case letters of the english alphabet, numbers and underscores (_)."\
                      " Name must start with a letter."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_009_add_geoobject_with_incorrect_name2():
    f = add_geoobject("incorrectS", "_label", "1")
    message = get_text(f, Metadata.name_label)
    assert message == "Name must contain only lower case letters of the english alphabet, numbers and underscores (_)."\
                      " Name must start with a letter."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_010_add_geoobject_with_long_name():
    f = add_geoobject("a"*41, "", "1")
    message = get_text(f, Metadata.name_label)
    assert message == "Maximum number of characters is 40."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_011_add_geoobject_with_long_label():
    f = add_geoobject("", "a"*41, "1")
    message = get_text(f, Metadata.label_label)
    assert message == "Maximum number of characters is 40."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_012_add_geoobject_with_short_name():
    f = add_geoobject("a", "", "1")
    message = get_text(f, Metadata.name_label)
    assert message == "Minimal number of characters is 2."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_013_add_geoobject_with_short_label():
    f = add_geoobject("", "a", "1")
    message = get_text(f, Metadata.label_label)
    assert message == "Minimal number of characters is 2."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_014_add_geoobject_with_empty_name_and_label():
    f = add_geoobject("a", "a", "1")
    delete = u'\ue009' + u'\ue003'
    input_values(f, Metadata.name, delete)
    input_values(f, Metadata.label, delete)
    time.sleep(1)
    message1 = get_text(f, Metadata.name_label)
    message2 = get_text(f, Metadata.label_label)
    assert message1 == "Name is required"
    assert message2 == "Label is required"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_015_add_geoobject_linestring_incorrect():
    f = add_geoobject("correct_namee", "_label", "3")
    input_values(f, GeoobjectLocators.min_point_count, "1")
    input_values(f, GeoobjectLocators.max_point_count, "0")
    time.sleep(1)
    messages = f.find_elements_by_css_selector(GeoobjectLocators.min_max_point_count_label)
    min_point_message = messages[0].text
    max_point_message = messages[1].text
    assert min_point_message == "Min. point count must be minimum 2"
    assert max_point_message == "Max. point count must be minimum 1"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_016_add_geoobject_multilinestring_incorrect():
    f = add_geoobject("correct_namee", "_label", "4")
    input_values(f, GeoobjectLocators.min_point_count, "1")
    input_values(f, GeoobjectLocators.max_point_count, "0")
    time.sleep(1)
    messages = f.find_elements_by_css_selector(GeoobjectLocators.min_max_point_count_label)
    min_point_message = messages[0].text
    max_point_message = messages[1].text
    assert min_point_message == "Min. point count must be minimum 2"
    assert max_point_message == "Max. point count must be minimum 1"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_017_add_geoobject_polygon_incorrect():
    f = add_geoobject("correct_namee", "_label", "5")
    input_values(f, GeoobjectLocators.min_point_count, "1")
    input_values(f, GeoobjectLocators.max_point_count, "0")
    time.sleep(1)
    messages = f.find_elements_by_css_selector(GeoobjectLocators.min_max_point_count_label)
    min_point_message = messages[0].text
    max_point_message = messages[1].text
    assert min_point_message == "Min. point count must be minimum 3"
    assert max_point_message == "Max. point count must be minimum 1"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_018_add_geoobject_multipolygon_incorrect():
    f = add_geoobject("correct_namee", "_label", "6")
    input_values(f, GeoobjectLocators.min_point_count, "1")
    input_values(f, GeoobjectLocators.max_point_count, "0")
    time.sleep(1)
    messages = f.find_elements_by_css_selector(GeoobjectLocators.min_max_point_count_label)
    min_point_message = messages[0].text
    max_point_message = messages[1].text
    assert min_point_message == "Min. point count must be minimum 3"
    assert max_point_message == "Max. point count must be minimum 1"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_019_add_and_edit_and_delete_multiselect_field():
    f = add_multiselect_field_in_new_geoobject("multiselect", "label", "help")
    click_on_element(f, Multiselectfield.domaintype)
    time.sleep(1)
    click_on_element(f, Multiselectfield.domaintype_value)
    time.sleep(1)
    click_on_element(f, Multiselectfield.source_table)
    time.sleep(1)
    elements = f.find_elements_by_css_selector(".ant-select-item-option-content")
    length_list_elements = len(elements)
    elements[3].click()
    time.sleep(1)
    click_on_element(f, Multiselectfield.source_field)
    time.sleep(1)
    field_values = f.find_elements_by_css_selector(".ant-select-item-option-content")
    field_values[length_list_elements+1].click()
    input_values(f, Multiselectfield.minimum_number, "3")
    input_values(f, Multiselectfield.maximum_number, "12")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "multiselect")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "multiselect"
    assert name == "multiselect"
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, Multiselectfield.minimum_number, u'\ue009' + u'\ue003')
    input_values(f, Multiselectfield.minimum_number, "5")
    input_values(f, Multiselectfield.maximum_number, u'\ue009' + u'\ue003')
    input_values(f, Multiselectfield.maximum_number, "50")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "multiselect")
    click_on_element(f, FieldsParameterTable.edit)
    edited_min = get_value(f, Multiselectfield.minimum_number)
    edited_max = get_value(f, Multiselectfield.maximum_number)
    assert edited_min == "5"
    assert edited_max == "50"
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "multiselect")
    message = delete_field_in_geoobject(f, "multiselect")
    assert message == "No Data"
    delete_geoobject(f, "geo_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_020_add_multiselect_field_incorrect1():
    f = add_multiselect_field_in_new_geoobject("multiselect", "label", "help")
    input_values(f, Multiselectfield.minimum_number, "3")
    input_values(f, Multiselectfield.maximum_number, "2")
    message1 = get_text(f, Multiselectfield.minimum_number_label)
    message2 = get_text(f, Multiselectfield.maximum_number_label)
    assert message1 == "Value must be lower than maximum number and can't be 0 if allow empty is checked"
    assert message2 == "Must be greater or equal to minimum number"
    click_on_element(f, FieldsParameterTable.cancel)
    delete_geoobject(f, "geo_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_021_add_multiselect_field_incorrect2():
    f = add_multiselect_field_in_new_geoobject("multiselect", "label", "help")
    input_values(f, Multiselectfield.minimum_number, "0")
    input_values(f, Multiselectfield.maximum_number, "2")
    message1 = get_text(f, Multiselectfield.minimum_number_label)
    assert message1 == "Value must be lower than maximum number and can't be 0 if allow empty is checked"
    click_on_element(f, FieldsParameterTable.cancel)
    delete_geoobject(f, "geo_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_022_add_and_delete_file_field():
    f = add_field_in_geoobject("7", "file", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "file")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "file"
    assert name == "file"
    message = delete_field_in_geoobject(f, "file")
    assert message == "No Data"
    delete_geoobject(f, "s_788")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_023_add_and_delete_image_field():
    f = add_field_in_geoobject("9", "image", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "image")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "image"
    assert name == "image"
    message = delete_field_in_geoobject(f, "image")
    assert message == "No Data"
    delete_geoobject(f, "s_788")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_024_add_edit_and_delete_long_lat_alt_field():
    f = add_field_in_geoobject("15", "longlat", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "longlat")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "long/lat/alt"
    assert name == "longlat"
    input_values(f, ParameterTablesLocators.search, "longlat")
    click_on_element(f, FieldsGeoobject.edit)
    click_on_element(f, Multiselectfield.domaintype)
    time.sleep(1)
    click_on_element(f, LongLatAltField.lat)
    input_values(f, FieldsParameterTable.label, u'\ue009' + u'\ue003')
    input_values(f, FieldsParameterTable.label, "new_label")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "longlat")
    click_on_element(f, FieldsGeoobject.edit)
    label_value = get_value(f, FieldsParameterTable.label)
    click_on_element(f, FieldsParameterTable.save)
    assert "new_label" == label_value
    time.sleep(1)
    message = delete_field_in_geoobject(f, "longlat")
    assert message == "No Data"
    delete_geoobject(f, "s_788")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_025_add_edit_and_delete_xyz_field():
    f = add_field_in_geoobject("16", "xyz", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "xyz")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "x/y/z"
    assert name == "xyz"
    input_values(f, ParameterTablesLocators.search, "xyz")
    click_on_element(f, FieldsGeoobject.edit)
    click_on_element(f, Multiselectfield.domaintype)
    time.sleep(1)
    click_on_element(f, XYZField.y)
    input_values(f, FieldsParameterTable.label, u'\ue009' + u'\ue003')
    input_values(f, FieldsParameterTable.label, "new_label")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "xyz")
    click_on_element(f, FieldsGeoobject.edit)
    label_value = get_value(f, FieldsParameterTable.label)
    click_on_element(f, FieldsParameterTable.save)
    assert "new_label" == label_value
    time.sleep(1)
    message = delete_field_in_geoobject(f, "xyz")
    assert message == "No Data"
    delete_geoobject(f, "s_788")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_026_add_and_delete_full_index():
    driver, name_index = add_index_in_geoobject("aa_1")
    click_on_element(driver, Indexes.save)
    time.sleep(1)
    delete_index_in_geoobject(driver, name_index)
    message = get_text(driver, FieldsParameterTable.no_data)
    assert message == "No Data"
    driver.close()


@allure.severity(allure.severity_level.NORMAL)
def test_027_add_and_delete_and_delete_fields_in_indexes():
    driver, name_index = add_index_in_geoobject("aa_1")
    for i in range(3):
        driver.find_element_by_css_selector(Indexes.delete_field_three).click()
        time.sleep(0.5)
    driver.find_element_by_css_selector(Indexes.save).click()
    delete_index_in_geoobject(driver, name_index)
    message = driver.find_element_by_css_selector(FieldsParameterTable.no_data).text
    assert message == "No Data"
    driver.close()


@allure.severity(allure.severity_level.NORMAL)
def test_028_geoobject_finalization():
    f = add_field_in_geoobject("12", "char", "label", "help")
    input_values(f, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Charfield.max_length, "76")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsGeoobject.integer_field, "integer_name", "label", "helptext")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsGeoobject.decimal_field, "decimal_name", "label", "helptext")
    input_values(f, Decimalfield.digits, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.digits, "10")
    input_values(f, Decimalfield.decimal, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.decimal, "6")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsGeoobject.time_field, "timefield", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsGeoobject.date_time_field, "datetime", "label", "helptext")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsParameterTable.textarea_field, "textarea", "label", "helptext")
    input_values(f, Textfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Textfield.max_length, "405")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    f.find_element_by_xpath("//*[@id='3']").click()
    click_on_element(f, Finalization.finalize_button)
    click_on_element(f, Metadata.save_changes_yes)
    click_on_element(f, DataModelPageLocators.geoobject)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "s789")
    time.sleep(1)
    finished = f.find_element_by_css_selector(ParameterTablesLocators.finished).is_enabled()
    assert finished is True
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_029_geoobject_update_form():
    f, message_first_section, message_first_section_finalize, display_type, form_type = \
        update_form("geoobject_with_4_field")
    assert display_type == "Inline"
    assert form_type == "Update"
    assert message_first_section == "section_name1"
    assert message_first_section_finalize == "section_name1label"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_030_geoobject_create_form():
    f, message_first_section, message_first_section_finalize, display_type, form_type = \
        create_form("geoobject_with_4_field")
    assert display_type == "Tabs"
    assert form_type == "Create"
    assert message_first_section == "section_name1"
    assert message_first_section_finalize == "section_name1label"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_031_geoobject_edit_update_form():
    f, message_first_section, display_type, form_type = edit_update_form("tabela_geo")
    assert message_first_section == "section_name1_new"
    assert display_type == "Tabs"
    assert form_type == "Update"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_032_geoobject_edit_create_form():
    f, message_first_section, display_type, form_type = edit_create_form("tabela_geo")
    assert message_first_section == "section_name1_new"
    assert display_type == "Inline"
    assert form_type == "Create"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_033_geoobjects_columns_and_filters():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.geoobject)
    input_values(f, ParameterTablesLocators.search, "def_kfj152")
    time.sleep(1)
    click_on_element(f, GeoobjectLocators.columns_and_filters)
    time.sleep(1)
    for counter in range(4):
        source = f.find_element_by_css_selector(ColumnsFilters.labels_from_first_place)
        target = f.find_element_by_css_selector(ColumnsFilters.labels_to_central_place)
        ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        time.sleep(1)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    for counter in range(2):
        source = f.find_element_by_css_selector(ColumnsFilters.labels_from_first_place)
        target = f.find_element_by_css_selector(ColumnsFilters.labels_to_central_place)
        ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        time.sleep(1)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    first_filter_in_view = get_text(f, ColumnsFilters.view_filters_first)
    second_filter_in_view = get_text(f, ColumnsFilters.view_columns_second)
    # assert first_filter_in_view == "textarealabel"
    # assert second_filter_in_view == "datetimelabel"
    print(first_filter_in_view, second_filter_in_view)
    first_column_in_view = get_text(f, ColumnsFilters.view_columns_first)
    second_filter_in_view = get_text(f, ColumnsFilters.view_columns_second)
    # assert first_column_in_view == "textarealabel"
    # assert second_filter_in_view == "datetimelabel"
    print(first_column_in_view, second_filter_in_view)  
    click_on_element(f, ColumnsFilters.next_finalize_button)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "def_kfj152")
    time.sleep(1)
    click_on_element(f, GeoobjectLocators.columns_and_filters)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    time.sleep(0.5)
    first_filter_in_view2 = get_text(f, ColumnsFilters.view_filters_first)
    second_filter_in_view2 = get_text(f, ColumnsFilters.view_filters_second)
    # assert first_filter_in_view2 == "textarealabel"
    # assert second_filter_in_view2 == "datetimelabel"
    print(first_filter_in_view2, second_filter_in_view2)
    time.sleep(0.5)
    first_column_in_view2 = get_text(f, ColumnsFilters.view_columns_first)
    second_filter_in_view2 = get_text(f, ColumnsFilters.view_columns_second)
    # assert first_column_in_view2 == "textarealabel"
    # assert second_filter_in_view2 == "datetimelabel"
    print(first_column_in_view2, second_filter_in_view2)
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_034_add_edit_delete_closest_field():
    f = add_field_in_geoobject("19", "closest", "label", "help")
    click_on_element(f, GeoRefSelectfield.source_table)
    field_values = f.find_elements_by_css_selector(".ant-select-item-option-content")
    time.sleep(1)
    click_on_element(f, GeoRefSelectfield.source_table_value)
    click_on_element(f, GeoRefSelectfield.source_field)
    time.sleep(1)
    field_values2 = f.find_elements_by_css_selector(".ant-select-item-option-content")
    field_values2[len(field_values) + 1].click()
    input_values(f, GeoRefSelectfield.max_distance, "1")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "closest")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "geometry_closest"
    assert name == "closest"
    click_on_element(f, FieldsObject.edit)
    click_on_element(f, Multiselectfield.domaintype)
    time.sleep(1)
    input_values(f, GeoRefSelectfield.max_distance, u'\ue009' + u'\ue003')
    input_values(f, GeoRefSelectfield.max_distance, "10")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "closest")
    click_on_element(f, FieldsGeoobject.edit)
    max_value = get_value(f, GeoRefSelectfield.max_distance)
    click_on_element(f, FieldsParameterTable.save)
    assert max_value == "10"
    message = delete_field_in_geoobject(f, "closest")
    assert message == "No Data"
    delete_geoobject(f, "s788")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_035_add_edit_delete_contains_field():
    f = add_field_in_geoobject("20", "contains", "label", "help")
    click_on_element(f, GeoRefSelectfield.source_table)
    field_values = f.find_elements_by_css_selector(".ant-select-item-option-content")
    time.sleep(1)
    click_on_element(f, GeoRefSelectfield.source_table_value)
    click_on_element(f, GeoRefSelectfield.source_field)
    time.sleep(1)
    field_values2 = f.find_elements_by_css_selector(".ant-select-item-option-content")
    field_values2[len(field_values) + 1].click()
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "contains")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "geometry_contains"
    assert name == "contains"
    click_on_element(f, FieldsObject.edit)
    time.sleep(1)
    input_values(f, FieldsParameterTable.label, u'\ue009' + u'\ue003')
    input_values(f, FieldsParameterTable.label, "new_label")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "contains")
    click_on_element(f, FieldsGeoobject.edit)
    label_value = get_value(f, FieldsParameterTable.label)
    click_on_element(f, FieldsParameterTable.save)
    assert label_value == "new_label"
    message = delete_field_in_geoobject(f, "contains")
    assert message == "No Data"
    delete_geoobject(f, "s788")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_036_add_edit_delete_intersects_field():
    f = add_field_in_geoobject("21", "intersects", "label", "help")
    click_on_element(f, GeoRefSelectfield.source_table)
    field_values = f.find_elements_by_css_selector(".ant-select-item-option-content")
    time.sleep(1)
    click_on_element(f, GeoRefSelectfield.source_table_value)
    click_on_element(f, GeoRefSelectfield.source_field)
    time.sleep(1)
    field_values2 = f.find_elements_by_css_selector(".ant-select-item-option-content")
    field_values2[len(field_values) + 1].click()
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "intersects")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "geometry_intersects"
    assert name == "intersects"
    click_on_element(f, FieldsObject.edit)
    time.sleep(1)
    input_values(f, FieldsParameterTable.label, u'\ue009' + u'\ue003')
    input_values(f, FieldsParameterTable.label, "new_label")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "intersects")
    click_on_element(f, FieldsGeoobject.edit)
    label_value = get_value(f, FieldsParameterTable.label)
    click_on_element(f, FieldsParameterTable.save)
    assert label_value == "new_label"
    message = delete_field_in_geoobject(f, "intersects")
    assert message == "No Data"
    delete_geoobject(f, "s788")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_037_add_edit_delete_within_field():
    f = add_field_in_geoobject("22", "within", "label", "help")
    click_on_element(f, GeoRefSelectfield.source_table)
    field_values = f.find_elements_by_css_selector(".ant-select-item-option-content")
    time.sleep(1)
    click_on_element(f, GeoRefSelectfield.source_table_value)
    click_on_element(f, GeoRefSelectfield.source_field)
    time.sleep(1)
    field_values2 = f.find_elements_by_css_selector(".ant-select-item-option-content")
    field_values2[len(field_values) + 1].click()
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "within")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "geometry_within"
    assert name == "within"
    click_on_element(f, FieldsObject.edit)
    time.sleep(1)
    input_values(f, FieldsParameterTable.label, u'\ue009' + u'\ue003')
    input_values(f, FieldsParameterTable.label, "new_label")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "within")
    click_on_element(f, FieldsGeoobject.edit)
    label_value = get_value(f, FieldsParameterTable.label)
    click_on_element(f, FieldsParameterTable.save)
    assert label_value == "new_label"
    message = delete_field_in_geoobject(f, "within")
    assert message == "No Data"
    delete_geoobject(f, "s788")
    f.close()
