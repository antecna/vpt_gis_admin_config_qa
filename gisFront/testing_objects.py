from source_objects import *
import allure
import pytest
import time


@allure.severity(allure.severity_level.NORMAL)
def test_001_add_object_correct():
    f = add_object("correct_namee", "_label")
    click_on_element(f, Metadata.next)
    click_on_element(f, Metadata.save_changes_yes)
    delete_object(f, "correct_namee")
    input_values(f, ParameterTablesLocators.search, "correct_namee")
    message = get_text(f, FieldsParameterTable.no_data)
    assert message == "No Data"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_002_add_object_with_incorrect_name():
    f = add_object("incorrec1/*", "_label")
    message = get_text(f, Metadata.name_label)
    assert message == "Name must contain only lower case letters of the english alphabet, numbers and underscores (_)."\
                      " Name must start with a letter."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_003_add_object_with_incorrect_name2():
    f = add_object("incorrectS", "_label")
    message = get_text(f, Metadata.name_label)
    assert message == "Name must contain only lower case letters of the english alphabet, numbers and underscores (_)."\
                      " Name must start with a letter."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_004_add_object_with_long_name():
    f = add_object("a"*41, "")
    message = get_text(f, Metadata.name_label)
    assert message == "Maximum number of characters is 40."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_005_add_object_with_long_label():
    f = add_object("", "a"*41)
    message = get_text(f, Metadata.label_label)
    assert message == "Maximum number of characters is 40."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_006_add_object_with_short_name():
    f = add_object("a", "")
    message = get_text(f, Metadata.name_label)
    assert message == "Minimal number of characters is 2."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_007_add_object_with_short_label():
    f = add_object("", "a")
    message = get_text(f, Metadata.label_label)
    assert message == "Minimal number of characters is 2."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_008_add_object_with_empty_name_and_label():
    f = add_object("a", "a")
    delete = u'\ue009' + u'\ue003'
    input_values(f, Metadata.name, delete)
    input_values(f, Metadata.label, delete)
    time.sleep(1)
    message1 = get_text(f, Metadata.name_label)
    message2 = get_text(f, Metadata.label_label)
    assert message1 == "Name is required"
    assert message2 == "Label is required"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_009_add_and_edit_and_delete_autonumber_field():
    f = add_field_in_object("1", "autonumber", "label", "autonumber_help")
    input_values(f, Autonumberfield.numbers_to_display, "5")
    time.sleep(1)
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "autonumber")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "autonumber"
    assert name == "autonumber"
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, Autonumberfield.numbers_to_display, u'\ue009' + u'\ue003')
    input_values(f, Autonumberfield.numbers_to_display, "10")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "autonumber")
    click_on_element(f, FieldsParameterTable.edit)
    padding = get_value(f, Autonumberfield.numbers_to_display)
    assert padding == "10"
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "autonumber")
    time.sleep(1)
    message = delete_field_in_object(f, "autonumber")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_010_add_autonumber_field_incorrect():
    f = add_field_in_object("1", "autonumber", "label", "autonumber_help")
    input_values(f, Autonumberfield.numbers_to_display, u'\ue009' + u'\ue003')
    input_values(f, Autonumberfield.numbers_to_display, "-5")
    message = get_text(f, Autonumberfield.numbers_to_display_label)
    click_on_element(f, FieldsParameterTable.cancel)
    assert message == "Field is required"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_011_add_and_delete_boolean_field():
    f = add_field_in_object("2", "boolean", "label", "boolean_help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "boolean")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "checkbox"
    assert name == "boolean"
    message = delete_field_in_object(f, "boolean")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_012_add_and_delete_date_field():
    f = add_field_in_object("3", "date", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "date")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "date"
    assert name == "date"
    message = delete_field_in_object(f, "date")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_013_add_and_delete_datetime_field():
    f = add_field_in_object("4", "date_and_time", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "date_and_time")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "datetime"
    assert name == "date_and_time"
    message = delete_field_in_object(f, "date_and_time")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_014_add_and_delete_number_float_field():
    f = add_field_in_object("7", "number_float", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "number_float")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "float"
    assert name == "number_float"
    message = delete_field_in_object(f, "number_float")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_015_add_and_delete_integer_field():
    f = add_field_in_object("9", "integer", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "integer")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "integer"
    assert name == "integer"
    message = delete_field_in_object(f, "integer")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_016_add_and_edit_and_delete_integer_slider_field():
    f = add_field_in_object("14", "integer_slider", "label", "help")
    input_values(f, Integerslider.min_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.min_value, "2")
    input_values(f, Integerslider.max_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.max_value, "95")
    input_values(f, Integerslider.step, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.step, "3")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "integer_slider")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "intslider"
    assert name == "integer_slider"
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, Integerslider.min_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.min_value, "8")
    input_values(f, Integerslider.max_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.max_value, "88")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "integer_slider")
    click_on_element(f, FieldsParameterTable.edit)
    edited_min_value = get_value(f, Integerslider.min_value)
    edited_max_value = get_value(f, Integerslider.max_value)
    assert edited_min_value == "8"
    assert edited_max_value == "88"
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "integer_slider")
    time.sleep(1)
    message = delete_field_in_object(f, "integer_slider")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_017_add_integer_slider_field_incorrect():
    f = add_field_in_object("14", "integer_slider", "label", "help")
    input_values(f, Integerslider.min_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.min_value, "100")
    input_values(f, Integerslider.max_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.max_value, "95")
    input_values(f, Integerslider.step, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.step, "200")
    message1 = get_text(f, Integerslider.min_value_label)
    message2 = get_text(f, Integerslider.max_value_label)
    message3 = get_text(f, Integerslider.step_label)
    assert message1 == "Value must be lower than maximum value"
    assert message2 == "Value must be greater than min value"
    assert message3 == "Value must be a positive number lower or equal to difference of min and max"
    click_on_element(f, FieldsParameterTable.cancel)
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_018_add_and_delete_char_field():
    f = add_field_in_object("11", "char", "label", "help")
    input_values(f, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Charfield.max_length, "76")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "char")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "text"
    assert name == "char"
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Charfield.max_length, "88")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "char")
    click_on_element(f, FieldsParameterTable.edit)
    edited_max_length = get_value(f, Charfield.max_length)
    assert edited_max_length == "88"
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "char")
    message = delete_field_in_object(f, "char")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_019_add_and_delete_char_field_incorrect():
    f = add_field_in_object("11", "char", "label", "help")
    input_values(f, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Charfield.max_length, "0")
    message1 = get_text(f, Charfield.max_length_label)
    assert message1 == "Field is required"
    click_on_element(f, FieldsParameterTable.cancel)
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_020_add_and_delete_time_field():
    f = add_field_in_object("13", "time", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "time")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "time"
    assert name == "time"
    message = delete_field_in_object(f, "time")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_021_add_decimal_field():
    f = add_field_in_object("5", "decimal", "decimallabel", "helpdec")
    input_values(f, Decimalfield.digits, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.digits, "10")
    input_values(f, Decimalfield.decimal, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.decimal, "6")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(0.5)
    input_values(f, ParameterTablesLocators.search, "decimal")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "decimal"
    assert name == "decimal"
    message = delete_field_in_object(f, "decimal")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_022_add_and_delete_file_field():
    f = add_field_in_object("6", "file", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "file")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "file"
    assert name == "file"
    message = delete_field_in_object(f, "file")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_023_add_and_delete_image_field():
    f = add_field_in_object("8", "image", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "image")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "image"
    assert name == "image"
    message = delete_field_in_object(f, "image")
    assert message == "No Data"
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_024_add_and_edit_and_delete_text_field():
    f = add_field_in_object("12", "text", "label", "help")
    input_values(f, Textfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Textfield.max_length, "405")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "text")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "textarea"
    assert name == "text"
    time.sleep(1)
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, Textfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Textfield.max_length, "100")
    click_on_element(f, FieldsParameterTable.save)
    delete_object(f, "object1")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_025_add_and_delete_full_index():
    driver, name_index = add_index_in_object("objekat")
    click_on_element(driver, Indexes.save)
    time.sleep(1)
    delete_index_in_object(driver, name_index)
    message = get_text(driver, FieldsParameterTable.no_data)
    assert message == "No Data"
    driver.close()


@allure.severity(allure.severity_level.NORMAL)
def test_027_add_and_delete_and_delete_fields_in_indexes():
    driver, name_index = add_index_in_object("objekaat")
    for i in range(3):
        driver.find_element_by_css_selector(Indexes.delete_field_three).click()
        time.sleep(0.5)
    driver.find_element_by_css_selector(Indexes.save).click()
    delete_index_in_object(driver, name_index)
    message = driver.find_element_by_css_selector(FieldsParameterTable.no_data).text
    assert message == "No Data"
    driver.close()


@allure.severity(allure.severity_level.NORMAL)
def test_028_object_finalization():
    f = add_field_in_object("11", "char", "label", "help")
    input_values(f, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Charfield.max_length, "76")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsObject.integer_field, "integer_name", "label", "helptext")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsObject.decimal_field, "decimal_name", "label", "helptext")
    input_values(f, Decimalfield.digits, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.digits, "10")
    input_values(f, Decimalfield.decimal, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.decimal, "6")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsObject.time_field, "timefield", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsObject.date_time_field, "datetime", "label", "helptext")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    f.find_element_by_xpath("//*[@id='3']").click()
    click_on_element(f, Finalization.finalize_button)
    click_on_element(f, Metadata.save_changes_yes)
    click_on_element(f, DataModelPageLocators.object)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "s790")
    time.sleep(1)
    finished = f.find_element_by_css_selector(ParameterTablesLocators.finished).is_enabled()
    assert finished is True
    f.close(),


@allure.severity(allure.severity_level.NORMAL)
def test_029_geoobjects_columns_and_filters():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.object)
    input_values(f, ParameterTablesLocators.search, "s790")
    time.sleep(1)
    click_on_element(f, ObjectLocators.columns_and_filters)
    time.sleep(1)
    for counter in range(4):
        source = f.find_element_by_css_selector(ColumnsFilters.labels_from_first_place)
        target = f.find_element_by_css_selector(ColumnsFilters.labels_to_central_place)
        ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        time.sleep(1)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    for counter in range(2):
        source = f.find_element_by_css_selector(ColumnsFilters.labels_from_first_place)
        target = f.find_element_by_css_selector(ColumnsFilters.labels_to_central_place)
        ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        time.sleep(1)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    first_filter_in_view = get_text(f, ColumnsFilters.view_filters_first)
    second_filter_in_view = get_text(f, ColumnsFilters.view_columns_second)
    assert first_filter_in_view == "datetimelabel"
    assert second_filter_in_view == "timefieldlabel"
    # print(first_filter_in_view, second_filter_in_view)
    first_column_in_view = get_text(f, ColumnsFilters.view_columns_first)
    second_filter_in_view = get_text(f, ColumnsFilters.view_columns_second)
    assert first_column_in_view == "datetimelabel"
    assert second_filter_in_view == "timefieldlabel"
    # print(first_column_in_view, second_filter_in_view)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "s790")
    time.sleep(1)
    click_on_element(f, ObjectLocators.columns_and_filters)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    time.sleep(0.5)
    first_filter_in_view2 = get_text(f, ColumnsFilters.view_filters_first)
    second_filter_in_view2 = get_text(f, ColumnsFilters.view_filters_second)
    assert first_filter_in_view2 == "datetimelabel"
    assert second_filter_in_view2 == "timefieldlabel"
    # print(first_filter_in_view2, second_filter_in_view2)
    time.sleep(0.5)
    first_column_in_view2 = get_text(f, ColumnsFilters.view_columns_first)
    second_filter_in_view2 = get_text(f, ColumnsFilters.view_columns_second)
    assert first_column_in_view2 == "datetimelabel"
    assert second_filter_in_view2 == "timefieldlabel"
    # print(first_column_in_view2, second_filter_in_view2)
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_030_object_update_form():
    f, message_first_section, message_first_section_finalize, display_type, form_type = \
        update_form("s790")
    assert display_type == "Inline"
    assert form_type == "Update"
    assert message_first_section == "section_name1"
    assert message_first_section_finalize == "section_name1label"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_031_geoobject_create_form():
    f, message_first_section, message_first_section_finalize, display_type, form_type = \
        create_form("s790")
    assert display_type == "Tabs"
    assert form_type == "Create"
    assert message_first_section == "section_name1"
    assert message_first_section_finalize == "section_name1label"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_032_geoobject_edit_update_form():
    f, message_first_section, display_type, form_type = edit_update_form("s789")
    assert message_first_section == "section_name1_new"
    assert display_type == "Tabs"
    assert form_type == "Update"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_033_geoobject_edit_create_form():
    f, message_first_section, display_type, form_type = edit_create_form("s789")
    assert message_first_section == "section_name1_new"
    assert display_type == "Inline"
    assert form_type == "Create"
    f.close()
