import time
import allure
import pytest
from fields import *
from selenium.webdriver.common.action_chains import ActionChains


@allure.severity(allure.severity_level.NORMAL)
def test_001_login_correct():
    f = login()
    message = get_text(f, ModulPageLocators.welcome)
    assert "Welcome" in message
    f.close()


@allure.severity(allure.severity_level.CRITICAL)
def test_002_login_incorrect():
    f = login("ad"*150, "a"*150)
    message = get_text(f, LoginPageLocators.labelButton)
    assert message == "Failed to login"
    f.close()


@allure.severity(allure.severity_level.CRITICAL)
def test_003_login_forget_name():
    f = login("", "pass")
    message = get_text(f, LoginPageLocators.labelName)
    assert message == "Username required"
    f.close()


@allure.severity(allure.severity_level.CRITICAL)
def test_004_login_forget_password():
    f = login("name", "")
    message = get_text(f, LoginPageLocators.labelPass)
    assert message == "Password required"
    f.close()


@allure.severity(allure.severity_level.CRITICAL)
def test_005_login_forget_name_and_password():
    f = login("", "")
    message1 = get_text(f, LoginPageLocators.labelName)
    assert message1 == "Username required"
    message2 = get_text(f, LoginPageLocators.labelPass)
    assert message2 == "Password required"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_006_changing_language_page2():
    f = lang()
    message = get_text(f, ModulPageLocators.welcome)
    assert "Dobrodošli" in message
    f.close()


@allure.severity(allure.severity_level.CRITICAL)
def test_007_logout_page2():
    f = login()
    click_on_element(f, ModulPageLocators.icon)
    time.sleep(1)
    click_on_element(f, ModulPageLocators.logout)
    a = "#root > div > form > div:nth-child(1) > div.sc-bdnxRM.sc-gtsrHT.sc-iCoGMd.fzUdiI.gfuSqG.dCTwEF"
    message = get_text(f, a)
    assert message == "Username"
    assert f.current_url == config.url
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_008_data_model_page2_click():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    assert f.current_url == config.url + "dataModel"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_009_model_antecna_click():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    click_on_element(f, DataModelPageLocators.dataModel_antecna)
    time.sleep(1)
    click_on_element(f, ".ant-dropdown-menu > a:nth-child(1)")
    assert f.current_url == config.url + "dataModel"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_010_parameter_tables_click():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    assert f.current_url == config.url + "dataModel/parameter-tables"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_011_geoobjects_click():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.geoobject)
    assert f.current_url == config.url + "dataModel/geoobjects"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_012_model_via_click():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    click_on_element(f, DataModelPageLocators.DataModel_Via)
    assert f.current_url == config.url + "dataModel"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_013_changing_language_page3():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, ModulPageLocators.icon)
    time.sleep(1)
    click_on_element(f, ModulPageLocators.language)
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_014_changing_languages_doubleclick():
    g = lang()
    click_on_element(g, ModulPageLocators.language)
    message = get_text(g, ModulPageLocators.welcome)
    assert "Welcome" in message
    g.close()


@allure.severity(allure.severity_level.NORMAL)
def test_015_logout_page3():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, ModulPageLocators.icon)
    time.sleep(1)
    click_on_element(f, ModulPageLocators.logout)
    name = get_text(f, "#root > div > form > div:nth-child(1) > div.sc-bdnxRM.sc-gtsrHT."
                       "sc-iCoGMd.fzUdiI.gfuSqG.dCTwEF")

    assert name == "Username"
    assert f.current_url == config.url
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_016_refresh_tables():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    time.sleep(1)
    message1 = get_text(f, ParameterTablesLocators.first_element)
    click_on_element(f, ParameterTablesLocators.page_number_two)
    time.sleep(1)
    click_on_element(f, ParameterTablesLocators.refresh)
    time.sleep(1)
    message2 = get_text(f, ParameterTablesLocators.first_element)
    assert message1 == message2
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_017_move_through_tables():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    time.sleep(1)
    message1 = get_text(f, ParameterTablesLocators.first_element)
    click_on_element(f, ParameterTablesLocators.page_number_two)
    time.sleep(1)
    click_on_element(f, ParameterTablesLocators.page_number_one)
    time.sleep(1)
    message2 = get_text(f, ParameterTablesLocators.first_element)
    assert message2 == message1
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_018_show_100_page():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    click_on_element(f, ParameterTablesLocators.show_page)
    time.sleep(1)
    click_on_element(f, ParameterTablesLocators.one_hundred_page)
    time.sleep(0.5)
    message = get_text(f, ParameterTablesLocators.show_page)
    assert message == "100 / page"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_019_add_table_correct():
    f = add_tables("correct_namee", "_label")
    click_on_element(f, Metadata.save_changes_yes)
    delete_table(f, "correct_namee")
    input_values(f, ParameterTablesLocators.search, "correct_namee")
    message = get_text(f, FieldsParameterTable.no_data)
    assert message == "No Data"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_020_add_table_with_incorrect_name():
    f = add_tables("incorrec1/*", "_label")
    message = get_text(f, Metadata.name_label)
    assert message == "Name must contain only lower case letters of the english alphabet, numbers and underscores (_)."\
                      " Name must start with a letter."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_021_add_table_with_incorrect_name2():
    f = add_tables("incorrectS", "_label")
    message = get_text(f, Metadata.name_label)
    assert message == "Name must contain only lower case letters of the english alphabet, numbers and underscores (_)."\
                      " Name must start with a letter."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_022_add_table_with_long_name():
    f = add_tables("a"*41, "")
    message = get_text(f, Metadata.name_label)
    assert message == "Maximum number of characters is 40."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_023_add_table_with_long_label():
    f = add_tables("", "a"*41)
    message = get_text(f, Metadata.label_label)
    assert message == "Maximum number of characters is 40."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_024_add_table_with_short_name():
    f = add_tables("a", "")
    message = get_text(f, Metadata.name_label)
    assert message == "Minimal number of characters is 2."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_025_add_table_with_short_label():
    f = add_tables("", "a")
    message = get_text(f, Metadata.label_label)
    assert message == "Minimal number of characters is 2."
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_026_add_table_with_empty_name_and_label():
    f = add_tables("a", "a")
    delete = u'\ue009' + u'\ue003'
    input_values(f, Metadata.name, delete)
    input_values(f, Metadata.label, delete)
    time.sleep(1)
    message1 = get_text(f, Metadata.name_label)
    message2 = get_text(f, Metadata.label_label)
    assert message1 == "Name is required"
    assert message2 == "Label is required"
    f.close()



@allure.severity(allure.severity_level.NORMAL)
def test_027_searh_by_name_correct():
    f = add_tables("newnametable", "label")
    click_on_element(f, Metadata.save_changes_yes)
    time.sleep(1)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, "newnametable")
    time.sleep(1)
    message = get_text(f, ParameterTablesLocators.first_element)
    assert "newnametable" in message
    input_values(f, ParameterTablesLocators.search, u'\ue009' + u'\ue003')
    delete_table(f, "newnametable")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_028_search_by_name_incorrect():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, "[,],&,+,^,},{,|,%,#")
    message = get_text(f, ParameterTablesLocators.noData)
    assert message == "No Data"
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_029_edit_in_tables():
    f = add_tables("nammme", "label")
    click_on_element(f, Metadata.save_changes_yes)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, "nammme")
    time.sleep(1)
    click_on_element(f, ParameterTablesLocators.edit)
    time.sleep(1)
    f.find_element_by_xpath("//*[@id='0']").click()
    input_values(f, Metadata.description, u'\ue009' + u'\ue003')
    input_values(f, Metadata.description, "Dodaj neki opis")
    click_on_element(f, Metadata.next)
    time.sleep(1)
    click_on_element(f, Metadata.save_changes_yes)
    time.sleep(1)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, "nammme")
    click_on_element(f, ParameterTablesLocators.edit)
    f.find_element_by_xpath("//*[@id='0']").click()
    message = get_text(f, Metadata.description)
    assert "Dodaj neki opis" in message
    delete_table(f, "nammme")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_030_add_decimal_field():
    f = add_field("5", "decimal", "decimallabel", "helpdec")
    input_values(f, Decimalfield.digits, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.digits, "10")
    input_values(f, Decimalfield.decimal, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.decimal, "6")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "decimal")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "decimal"
    assert name == "decimal"
    message = delete_field(f, "decimal")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@pytest.mark.skip
@allure.severity(allure.severity_level.NORMAL)
def test_031_add_decimal_field_in_new_table_long_name():
    f = add_field_in_new_table("a"*41, "label", "")
    message = get_text(f, Metadata.name_label)
    assert message == "Maximum number of characters is 40."


@pytest.mark.skip
@allure.severity(allure.severity_level.NORMAL)
def test_032_add_decimal_field_in_new_table_long_label():
    f = add_field_in_new_table("dec_field", "l"*41, "helptext")
    message = get_text(f, Metadata.label_label)
    assert message == "Maximum number of characters is 40."


@pytest.mark.skip
@allure.severity(allure.severity_level.NORMAL)
def test_033_add_decimal_field_in_new_table_long_help_text():
    f = add_field_in_new_table("dec_field", "l", "h"*201)
    message = get_text(f, Metadata.helptext_label)
    assert message == "Maximum number of characters is 200."


@allure.severity(allure.severity_level.NORMAL)
def test_034_add_decimal_field_in_new_table_incorrect():
    f = add_field_in_new_table("11111", "222", "help")
    message1 = get_text(f, Metadata.name_label)
    assert message1 == "Name must contain only lower case letters of the english alphabet," \
                       " numbers and underscores (_). Name must start with a letter."
    f.close()


@pytest.mark.skip
@allure.severity(allure.severity_level.NORMAL)
def test_035_add_and_delete_decimal_field_in_existing_table():
    name_field, label = "stefannnn", "lab"
    f = add_decimal_field_in_existing_table(name_field, label, "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, name_field)
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    click_on_element(f, FieldsParameterTable.delete)
    time.sleep(0.5)
    click_on_element(f, FieldsParameterTable.delete_yes)
    input_values(f, ParameterTablesLocators.search, name_field)
    message = get_text(f, FieldsParameterTable.no_data)
    assert typee == "decimal"
    assert name == name_field
    assert message == "No Data"
    f.close()


@pytest.mark.skip
@allure.severity(allure.severity_level.CRITICAL)
def test_036_add_existing_decimal_field_in_existing_table():
    f = add_decimal_field_in_existing_table("stefan", "label", "helptexzt")
    click_on_element(f, FieldsParameterTable.save)
    name = get_text(f, FieldsParameterTable.name)
    label = get_text(f, FieldsParameterTable.label)
    assert label == ""
    assert name == ""
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_037_add_decimal_field_in_existing_table_incorrect():
    f = add_decimal_field_in_existing_table("name12!=)(/&%$", "label55", "helptexzt")
    t = get_text(f, Metadata.name_label)
    assert t == "Name must contain only lower case letters of the english alphabet, numbers and underscores (_)." \
                " Name must start with a letter."
    f.close()


@pytest.mark.skip
@allure.severity(allure.severity_level.NORMAL)
def test_038_edit_decimal_field():
    old_name, old_label = "oldd", "label"
    new_name, new_label = "newq", "newl"
    f = add_decimal_field_in_existing_table(old_name, old_label, "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, old_name)
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, FieldsParameterTable.name, u'\ue009' + u'\ue003')
    time.sleep(0.5)
    input_values(f, FieldsParameterTable.name, new_name)
    input_values(f, FieldsParameterTable.label, u'\ue009' + u'\ue003')
    time.sleep(0.5)
    input_values(f, FieldsParameterTable.label, new_label)
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, new_label)
    time.sleep(0.5)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert name == new_name
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_039_add_and_edit_and_delete_text_field():
    f = add_field("9", "text", "label", "help")
    input_values(f, Textfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Textfield.max_length, "405")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "text")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "textarea"
    assert name == "text"
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, Textfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Textfield.max_length, "100")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "text")
    click_on_element(f, FieldsParameterTable.edit)
    edited_max_length = get_value(f, Textfield.max_length)
    assert edited_max_length == "100"
    time.sleep(1)
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "text")
    message = delete_field(f, "text")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_040_add_text_field_incorrect():
    f = add_field("9", "text", "label", "help")
    input_values(f, Textfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Textfield.max_length, "0")
    message1 = get_text(f, Textfield.max_length_label)
    assert message1 == "Field is required"
    click_on_element(f, FieldsParameterTable.cancel)
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_041_add_and_delete_select_field():
    f = add_field("12", "select", "label", "boolean_help")
    click_on_element(f, Selectfield.source_table)
    time.sleep(1)
    click_on_element(f, Selectfield.source_table_value)
    click_on_element(f, Selectfield.source_field)
    time.sleep(1)
    click_on_element(f, Selectfield.source_field_value)
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "select")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "select"
    assert name == "select"
    message = delete_field(f, "select")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_042_add_and_delete_full_index():
    driver, word = add_index("vasic")
    click_on_element(driver, Indexes.save)
    time.sleep(1)
    delete_index(driver, word)
    message = get_text(driver, FieldsParameterTable.no_data)
    assert message == "No Data"
    driver.close()


@allure.severity(allure.severity_level.NORMAL)
def test_043_add_and_delete_and_delete_fields_in_indexes():
    driver1, word = add_index("vasic")
    for i in range(3):
        driver1.find_element_by_css_selector(Indexes.delete_field_three).click()
        time.sleep(0.5)
    driver1.find_element_by_css_selector(Indexes.save).click()
    driver2 = delete_index(driver1, word)
    message = driver2.find_element_by_css_selector(FieldsParameterTable.no_data).text
    assert message == "No Data"
    driver2.close()


@allure.severity(allure.severity_level.NORMAL)
def test_044_add_and_edit_and_delete_autonumber_field():
    f = add_field("1", "autonumber", "label", "autonumber_help")
    input_values(f, Autonumberfield.numbers_to_display, "5")
    time.sleep(1)
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "autonumber")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "autonumber"
    assert name == "autonumber"
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, Autonumberfield.numbers_to_display, u'\ue009' + u'\ue003')
    input_values(f, Autonumberfield.numbers_to_display, "10")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "autonumber")
    click_on_element(f, FieldsParameterTable.edit)
    padding = get_value(f, Autonumberfield.numbers_to_display)
    assert padding == "10"
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "autonumber")
    time.sleep(1)
    message = delete_field(f, "autonumber")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_045_add_autonumber_field_incorrect():
    f = add_field("1", "autonumber", "label", "autonumber_help")
    input_values(f, Autonumberfield.numbers_to_display, u'\ue009' + u'\ue003')
    input_values(f, Autonumberfield.numbers_to_display, "-5")
    message = get_text(f, Autonumberfield.numbers_to_display_label)
    click_on_element(f, FieldsParameterTable.cancel)
    assert message == "Field is required"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_046_add_and_delete_boolean_field():
    f = add_field("2", "boolean", "label", "boolean_help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "boolean")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "checkbox"
    assert name == "boolean"
    message = delete_field(f, "boolean")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_047_add_and_delete_date_field():
    f = add_field("3", "date", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "date")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "date"
    assert name == "date"
    message = delete_field(f, "date")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_048_add_and_delete_datetime_field():
    f = add_field("4", "date_and_time", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "date_and_time")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "datetime"
    assert name == "date_and_time"
    message = delete_field(f, "date_and_time")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_049_add_and_delete_number_float_field():
    f = add_field("6", "number_float", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "number_float")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "float"
    assert name == "number_float"
    message = delete_field(f, "number_float")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_050_add_and_delete_integer_field():
    f = add_field("7", "integer", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "integer")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "integer"
    assert name == "integer"
    message = delete_field(f, "integer")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_051_add_and_edit_and_delete_integer_slider_field():
    f = add_field("11", "integer_slider", "label", "help")
    input_values(f, Integerslider.min_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.min_value, "2")
    input_values(f, Integerslider.max_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.max_value, "95")
    input_values(f, Integerslider.step, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.step, "3")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "integer_slider")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "intslider"
    assert name == "integer_slider"
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, Integerslider.min_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.min_value, "8")
    input_values(f, Integerslider.max_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.max_value, "88")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "integer_slider")
    click_on_element(f, FieldsParameterTable.edit)
    edited_min_value = get_value(f, Integerslider.min_value)
    edited_max_value = get_value(f, Integerslider.max_value)
    assert edited_min_value == "8"
    assert edited_max_value == "88"
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "integer_slider")
    time.sleep(1)
    message = delete_field(f, "integer_slider")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_052_add_integer_slider_field_incorrect():
    f = add_field("11", "integer_slider", "label", "help")
    input_values(f, Integerslider.min_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.min_value, "100")
    input_values(f, Integerslider.max_value, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.max_value, "95")
    input_values(f, Integerslider.step, u'\ue009' + u'\ue003')
    input_values(f, Integerslider.step, "200")
    message1 = get_text(f, Integerslider.min_value_label)
    message2 = get_text(f, Integerslider.max_value_label)
    message3 = get_text(f, Integerslider.step_label)
    assert message1 == "Value must be lower than maximum value"
    assert message2 == "Value must be greater than min value"
    assert message3 == "Value must be a positive number lower or equal to difference of min and max"
    click_on_element(f, FieldsParameterTable.cancel)
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_053_add_and_delete_char_field():
    f = add_field("8", "char", "label", "help")
    input_values(f, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Charfield.max_length, "76")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "char")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "text"
    assert name == "char"
    click_on_element(f, FieldsParameterTable.edit)
    input_values(f, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Charfield.max_length, "88")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "char")
    click_on_element(f, FieldsParameterTable.edit)
    edited_max_length = get_value(f, Charfield.max_length)
    assert edited_max_length == "88"
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "char")
    message = delete_field(f, "char")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_054_add_and_delete_char_field_incorrect():
    f = add_field("8", "char", "label", "help")
    input_values(f, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Charfield.max_length, "0")
    message1 = get_text(f, Charfield.max_length_label)
    assert message1 == "Field is required"
    click_on_element(f, FieldsParameterTable.cancel)
    delete_table(f, "tab_name")
    f.close()


@allure.severity(allure.severity_level.NORMAL)
def test_055_add_and_delete_time_field():
    f = add_field("10", "time", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    input_values(f, ParameterTablesLocators.search, "time")
    typee = get_text(f, FieldsParameterTable.first_el_data_type)
    name = get_text(f, FieldsParameterTable.first_el_name)
    assert typee == "time"
    assert name == "time"
    message = delete_field(f, "time")
    assert message == "No Data"
    delete_table(f, "tab_name")
    f.close()


@pytest.mark.skip
@allure.severity(allure.severity_level.NORMAL)
def test_056_table_finalization():
    f = add_field("8", "char", "label", "help")
    input_values(f, Charfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Charfield.max_length, "76")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsParameterTable.integer_field, "integer_name", "label", "helptext")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsParameterTable.decimal_field, "decimal_name", "label", "helptext")
    input_values(f, Decimalfield.digits, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.digits, "10")
    input_values(f, Decimalfield.decimal, u'\ue009' + u'\ue003')
    input_values(f, Decimalfield.decimal, "6")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsParameterTable.time_field, "timefield", "label", "help")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsParameterTable.date_time_field, "datetime", "label", "helptext")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    add_field_for_finalization(f, FieldsParameterTable.textarea_field, "textarea", "label", "helptext")
    input_values(f, Textfield.max_length, u'\ue009' + u'\ue003')
    input_values(f, Textfield.max_length, "405")
    click_on_element(f, FieldsParameterTable.save)
    time.sleep(1)
    f.find_element_by_xpath("//*[@id='3']").click()
    click_on_element(f, Finalization.finalize_button)
    click_on_element(f, Metadata.save_changes_yes)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, "tabfin_name")
    time.sleep(1)
    finished = f.find_element_by_css_selector(ParameterTablesLocators.finished).is_enabled()
    assert finished is True
    f.close()


def test_057_columns_and_filters_finalization():
    f = login()
    click_on_element(f, ModulPageLocators.modul_DataModel)
    click_on_element(f, DataModelPageLocators.parametarTabel)
    input_values(f, ParameterTablesLocators.search, "last_table")
    time.sleep(1)
    click_on_element(f, ParameterTablesLocators.columns_and_filters)
    time.sleep(1)
    for counter in range(4):
        source = f.find_element_by_css_selector(ColumnsFilters.labels_from_first_place)
        target = f.find_element_by_css_selector(ColumnsFilters.labels_to_central_place)
        ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        time.sleep(1)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    for counter in range(2):
        source = f.find_element_by_css_selector(ColumnsFilters.labels_from_first_place)
        target = f.find_element_by_css_selector(ColumnsFilters.labels_to_central_place)
        ActionChains(f).drag_and_drop(source, target).pause(1).perform()
        time.sleep(1)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    first_filter_in_view = get_text(f, ColumnsFilters.view_filters_first)
    second_filter_in_view = get_text(f, ColumnsFilters.view_columns_second)
    assert first_filter_in_view == "textarealabel"
    assert second_filter_in_view == "datetimelabel"
    first_column_in_view = get_text(f, ColumnsFilters.view_columns_first)
    second_filter_in_view = get_text(f, ColumnsFilters.view_columns_second)
    assert first_column_in_view == "textarealabel"
    assert second_filter_in_view == "datetimelabel"
    click_on_element(f, ColumnsFilters.next_finalize_button)
    time.sleep(1)
    input_values(f, ParameterTablesLocators.search, "last_table")
    time.sleep(1)
    click_on_element(f, ParameterTablesLocators.columns_and_filters)
    time.sleep(1)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    click_on_element(f, ColumnsFilters.next_finalize_button)
    first_filter_in_view2 = get_text(f, ColumnsFilters.view_filters_first)
    second_filter_in_view2 = get_text(f, ColumnsFilters.view_filters_second)
    assert first_filter_in_view2 == "textarealabel"
    assert second_filter_in_view2 == "datetimelabel"
    first_column_in_view2 = get_text(f, ColumnsFilters.view_columns_first)
    second_filter_in_view2 = get_text(f, ColumnsFilters.view_columns_second)
    assert first_column_in_view2 == "textarealabel"
    assert second_filter_in_view2 == "datetimelabel"
    f.close()


def test_058_parameter_table_create_form():
    f, message_first_section, message_first_section_finalize, display_type, form_type = create_form_table("novaptabela")
    assert display_type == "Inline"
    assert form_type == "Create"
    assert message_first_section == "section_name1"
    assert message_first_section_finalize == "section_name1label"
    f.close()


def test_059_parameter_table_update_form():
    f, message_first_section, message_first_section_finalize, display_type, form_type = update_form_table("novaptabela")
    assert display_type == "Inline"
    assert form_type == "Update"
    assert message_first_section == "section_name1"
    assert message_first_section_finalize == "section_name1label"
    f.close()
